# Client-server chat application #

## Application features: ##

* registration and authorization using HTTP request and Websocket;
* the password is stored as a hashcode with salt;
* "forgot password" function is available;
* sending messages to general chat and personal ones using Servlet (Jetty);
* chats history;
* creating private chats, inviting chat users;
* a list of all chat users on the chat page, those who are online are marked with green circles;
* architecture patterns used: Factory and Singletone;
<br /><br />

![picture](https://bitbucket.org/NastyaVa/wonderwomen_chat_backend/raw/80a7a9120ef57f90c16ad9ec9449d0db71902ebb/auth_and_chat_pages.png)


### Request for registration ###

- URL: http://localhost:9080/registration;
- requestType: POST;
- bodyType: json;
- requestBody:
  * String login, not null, latin characters, min size 4, max size 15, require;
  * String password, not null, latin characters, min size 8, max size 25, require;
  * String email, not null, latin characters (should have "@" between characters, characters after address sign, then dot and characters after dot), min size 5, require;
  * String phone_number, numbers and (not mandatory) special characters: - _():=+, min size 10, max size 14;
  * String company_name, min size 3, max size 15;

- valid request example
  "{
  "login" :   "Mike",
  "password" : "qwerty1234",
  "email" :  "qwerty@mail.ru",
  "phone_number" : "0786688687",
  "company_name" : "apple"
  }"

-valid response example:
Status: 200 OK;
body: "You are registered."
location: http://localhost:9080/registration

-invalid request example:
"{
"login" :  "Mike",
"password" : "qwerty1234",
"email" :  "qwerty@mail.ru",
"phone_number" : "088-SUPPORT",
"company_name" : "apple"
}"

-invalid response example  
Status: 403 Forbidden , body
"The phone must contain only numbers. Special characters available '- _():=+'."

### Request for authorization ###

- URL: http://localhost:9080/login;
- requestType: POST;
- bodyType: json;
- requestBody:
  * String login, not null, latin characters, min size 4, max size 15, require;
  * String password, not null, latin characters, min size 8, max size 15, require;

- valid request example:
  "{
  "login" :  "Mike",
  "password" : "qwerty1234",
  }"

-valid response example:
Status: 200 OK;
body - session uuid, example: "08abb857-0a48-4cc6-989e-fd9266fb49f8"

-invalid request example:
"{
"login" :  "Mik",
"password" : "qwerty1234",
}"

-invalid response example  
Status: 403 Forbidden , body
"Login must consist from letters and digits, and contain from 4 to 15 characters."