-- SCRIPT FOR DATABASE CREATION

CREATE DATABASE d6b5sq9md41rth
    WITH
    OWNER = rrjzcvupxmkxun
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.UTF-8'
    LC_CTYPE = 'en_US.UTF-8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1;

GRANT ALL ON DATABASE d6b5sq9md41rth TO rrjzcvupxmkxun;

-- SCRIPT FOR USERS TABLE CREATION

CREATE TABLE public."user"
(
    id integer NOT NULL DEFAULT nextval('user_id_seq'::regclass),
    login character varying COLLATE pg_catalog."default" NOT NULL,
    password character varying COLLATE pg_catalog."default" NOT NULL,
    email character varying COLLATE pg_catalog."default" NOT NULL,
    phone_number character varying COLLATE pg_catalog."default",
    company_name character varying COLLATE pg_catalog."default",
    CONSTRAINT user_pkey PRIMARY KEY (id),
    CONSTRAINT email UNIQUE (email),
    CONSTRAINT login UNIQUE (login)
)

    TABLESPACE pg_default;

ALTER TABLE public."user"
    OWNER to rrjzcvupxmkxun;

-- SCRIPT FOR USERS_CHATS TABLE CREATION

create table users_chats
(
    id int,
    login varchar not null,
    chat varchar
);

-- CREATE SEQUECE

CREATE SEQUENCE all_messages_sequence start 1000;
CREATE SEQUENCE user_chat_sequence start 500;