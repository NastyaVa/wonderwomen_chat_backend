package cache;

import models.LoginAndSession;
import org.eclipse.jetty.websocket.api.Session;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class SessionCache {

    private ConcurrentHashMap<UUID, LoginAndSession> cache;
    private final GetNewID getNewID;

    public SessionCache(GetNewID getNewID) {
        this.getNewID = getNewID;
        cache = new ConcurrentHashMap<>();
    }

    public UUID addCache(String login) {
        UUID uuid = getNewID.getUUID();
        cache.put(uuid, new LoginAndSession(login));
        return uuid;
    }

    public UUID setSessionAndUUID(Session session) {
        UUID uuid = UUID.fromString(session.getUpgradeRequest().getParameterMap().get("access-token").get(0));
        if (cache.get(uuid) == null) {
            return null;
        }
        if (cache.get(uuid).getSession() == null) {
            cache.get(uuid).setSession(session);
            return uuid;
        }
        UUID newSessionUUID = addCache(cache.get(uuid).getLogin());
        cache.get(newSessionUUID).setSession(session);
        return newSessionUUID;
    }

    public boolean isUserLoggedInMoreThanOnce(UUID uuid) {
        String login = cache.get(uuid).getLogin();
        int count = 0;
        for (LoginAndSession loginAndSession : cache.values()) {
            if (loginAndSession.getLogin().equals(login)) {
                count++;
                if (count > 1) {
                    return true;
                }
            }
        }
        return false;
    }

    public UUID getUUIDBySession(Session session) {
        for (UUID uuid : cache.keySet()) {
            if (cache.get(uuid).getSession() != null) {
                if (cache.get(uuid).getSession().equals(session)) {
                    return uuid;
                }
            }
        }

        return null;
    }

    public LoginAndSession findByUUID(UUID uuid) {
        return cache.get(uuid);
    }

    public void removeByUUID(UUID uuid) {
        cache.remove(uuid);

    }

    public ConcurrentHashMap<UUID, LoginAndSession> getCache() {
        return cache;
    }
}
