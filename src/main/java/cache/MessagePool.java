package cache;

import org.apache.log4j.Logger;
import models.MessageData;
import server.services.websocket.command_services.CommandServiceFabric;
import server.services.websocket.command_services.ICommandService;
import java.util.Objects;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;

public class MessagePool {

    private static final Logger log = Logger.getLogger(MessagePool.class);

    private CommandServiceFabric commandServiceFabric;
    private BlockingQueue<MessageData> messages = new LinkedBlockingQueue<>();

    private class MessagePoolStarter implements Runnable {

        @Override
        public void run() {
            while (!Thread.currentThread().isInterrupted()) {
                try {
                    MessageData messageData = messages.take();
                    ICommandService requestedService = commandServiceFabric.chooseSession(messageData.getCommand());
                    requestedService.sendMessage(messageData.getMessage(), messageData.getUuid());
                } catch (InterruptedException e) {
                    log.error(e.getMessage());
                }
            }
        }

    }

    public MessagePool(CommandServiceFabric commandServiceFabric) {
        this.commandServiceFabric = commandServiceFabric;
        Thread thread = new Thread(new MessagePoolStarter());
        thread.start();
    }

    public boolean addMessage(MessageData messageData) {
        return messages.add(messageData);
    }



    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MessagePool that = (MessagePool) o;
        return Objects.equals(messages, that.messages);
    }

    @Override
    public int hashCode() {
        return Objects.hash(messages);
    }
}
