package cache;

import java.util.UUID;

public class GetNewID {

    public UUID getUUID() {
        return UUID.randomUUID();
    }
}
