package database.subsidiary;

import java.util.Random;

public class RandomPassword {
    Random random;

    public RandomPassword(Random random){
        this.random = random;
    }

    public String getRandomPassword(String login){
        int randomNumber = random.nextInt(999999999);
        String newPassword = login.substring(0,1) + randomNumber + login.substring(login.length()-1);
        return newPassword;
    }
}
