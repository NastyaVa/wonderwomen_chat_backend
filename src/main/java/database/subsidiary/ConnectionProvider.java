package database.subsidiary;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import static utils.AppContext.getAppContext;

public class ConnectionProvider {

    public Connection getConnection() throws SQLException {
        try {
            Class.forName(getAppContext().getProperty().getDriver());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        return DriverManager.getConnection(getAppContext().getProperty().getHost(),
                getAppContext().getProperty().getLogin(),
                getAppContext().getProperty().getPassword());
    }

    public PreparedStatement getPreparedStatement(String sql) throws SQLException {
        return getConnection().prepareStatement(sql);
    }

}
