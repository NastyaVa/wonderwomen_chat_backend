package database.dao;

import models.User;
import java.util.List;

public interface IUserDAO {

    User findByLogin (String login);
    User findByEmail(String email);
    String changePassword(String login);
    Boolean createUser (User user);
    Boolean updateUser (User user, String login, String password, String email, String phoneNumber, String companyName);
    String authorization (String login);
    List<String> getAllUserLogins();

}
