package database.dao;

import database.subsidiary.ConnectionProvider;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import java.sql.*;
import static utils.constants.ConstantsDB.*;

public class PrivateChatsDAO {
    private final Logger logger = LogManager.getLogger(PrivateChatsDAO.class);
    private final ConnectionProvider connectionProvider;

    public PrivateChatsDAO(ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }


    public boolean addLoginsAndChatName(String tableName, String[] logins) {
        int length = logins.length;
        StringBuilder sqlRequest = new StringBuilder();
        sqlRequest.append(INSERT_USERS_IN_TABLE_USERS_AND_CHATS);
        for (int i = 0; i < length; i++) {
            sqlRequest.append(INSERT_USER_DATA);
            if (i != length - 1) {
                sqlRequest.append(",");
            } else {
                sqlRequest.append(";");
            }
        }
        if (createTable(tableName)) {
            try (PreparedStatement statement = connectionProvider.getConnection().prepareStatement(
                    sqlRequest.toString())) {
                int colNum = 0;
                for (String login : logins) {
                    statement.setString(colNum += 1, login);
                    statement.setString(colNum += 1, tableName);
                }
                statement.execute();
                return true;

            } catch (SQLException e) {
                logger.info(e.getMessage());
            }
        }
        return false;
    }

    public boolean checkIfChatNameExists(String tableName) {
        try (PreparedStatement statement = connectionProvider.getConnection()
                .prepareStatement(GET_ALL_CHAT_NAMES)) {
            statement.execute();
            ResultSet resultSet = statement.getResultSet();
            while (resultSet.next()) {
                if (tableName.equals(resultSet.getString("chat"))) {
                    return true;
                }
            }
            return false;
        } catch (SQLException throwable) {
            throwable.printStackTrace();
            return true;
        }
    }

    private boolean createTable(String tableName) {
        try (PreparedStatement statement = connectionProvider.getConnection()
                .prepareStatement(String.format(DB_CREATE_TABLE_PRIVAT_CHAT, tableName))) {
            statement.execute();
            logger.info(String.format(CREATE_TABLE, tableName));
            return true;
        } catch (SQLException e) {
            logger.info(e.getMessage());
            return false;
        }
    }
}