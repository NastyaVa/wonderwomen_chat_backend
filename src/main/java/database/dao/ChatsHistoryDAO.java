package database.dao;

import database.subsidiary.ConnectionProvider;
import models.Message;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;
import static utils.constants.ConstantsDB.*;

public class ChatsHistoryDAO {
    private final ConnectionProvider connectionProvider;
    private final Logger logger = LogManager.getLogger(ChatsHistoryDAO.class);

    public ChatsHistoryDAO(ConnectionProvider connectionProvider) {
        this.connectionProvider = connectionProvider;
    }

    public Boolean saveMessages(Message message) {
        int loginColumn = 1;
        int messageColumn = 2;
        int timeColumn = 3;

        if (!message.getFrom().equals(DB_COLUMN_LABEL_SERVER)) {
            try (PreparedStatement statement = connectionProvider.getConnection()
                    .prepareStatement(String.format(DB_ADD_TO_PRIVATE_HISTORY, message.getTo()))) {
                statement.setString(loginColumn, message.getFrom());
                statement.setString(messageColumn, message.getPayload());
                statement.setLong(timeColumn, message.getTime());
                statement.execute();
                return true;

            } catch (SQLException e) {
                logger.error(e.getMessage());
            }
        }
        return false;
    }

    public Queue<Message> getListMessages(String chatName) {
        int fromColumnNumber = 2;
        int payloadColumnNumber = 3;
        int timeColumnNumber = 4;

        Queue<Message> listMessages = new ArrayDeque<>();

        try (Statement statement = connectionProvider.getConnection().createStatement();
             ResultSet resultSet = statement.executeQuery(String.format(DB_READ_HISTORY, chatName))) {

            while (resultSet.next()) {
                listMessages.add(new Message(resultSet.getString(fromColumnNumber),
                        chatName, resultSet.getString(payloadColumnNumber),
                        resultSet.getLong(timeColumnNumber)));
            }

        } catch (SQLException e) {
            logger.error(e.getMessage());
        }

        return listMessages;
    }

    public List<String> getChatsByLogin(String login) {
        int chatColumnNumber = 1;
        List<String> chats = new ArrayList<>();

        try (PreparedStatement statement = connectionProvider.getConnection()
                .prepareStatement(DB_GET_ALL_CHATS_BY_LOGIN)) {
            statement.setString(1, login);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                chats.add(resultSet.getString(chatColumnNumber));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }

        return  chats;
    }

    public List<String> getLoginsByChatName(String chatName) {
        int loginColumnNumber = 1;
        List<String> chats = new ArrayList<>();

        try (PreparedStatement statement = connectionProvider.getConnection()
                .prepareStatement(DB_GET_ALL_LOGINS_BY_CHAT_NAME)) {
            statement.setString(1, chatName);
            ResultSet resultSet = statement.executeQuery();

            while (resultSet.next()) {
                chats.add(resultSet.getString(loginColumnNumber));
            }
        } catch (SQLException e) {
            logger.error(e.getMessage());
        }

        return chats;
    }


}