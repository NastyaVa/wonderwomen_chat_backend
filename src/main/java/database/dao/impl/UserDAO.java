package database.dao.impl;

import database.subsidiary.HashPassword;
import database.subsidiary.RandomPassword;
import org.apache.log4j.Logger;
import utils.constants.ConstantsLogger;
import database.subsidiary.ConnectionProvider;
import database.dao.IUserDAO;
import models.User;
import utils.constants.ConstantsUserData;
import utils.constants.ConstantsDB;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;

public class UserDAO implements IUserDAO {

    private static final Logger log = Logger.getLogger(UserDAO.class);
    private final ConnectionProvider connectionProvider;
    private final HashPassword hashPassword;
    private final RandomPassword randomPassword;

    public UserDAO(ConnectionProvider connectionProvider, HashPassword hashPassword, RandomPassword randomPassword) {
        this.connectionProvider = connectionProvider;
        this.hashPassword = hashPassword;
        this.randomPassword = randomPassword;
    }

    @Override
    public User findByLogin(String login) {
        try (PreparedStatement preparedStatement = connectionProvider.getPreparedStatement(ConstantsDB.FIND_BY_LOGIN)) {
                preparedStatement.setString(1, login);
                ResultSet resultSet = preparedStatement.executeQuery();
                User user = null;
                if (resultSet.next()) {
                    user = new User(resultSet.getString(ConstantsUserData.LOGIN),
                            resultSet.getString(ConstantsUserData.PASSWORD),
                            resultSet.getString(ConstantsUserData.EMAIL),
                            resultSet.getString(ConstantsUserData.PHONE_NUMBER),
                            resultSet.getString(ConstantsUserData.COMPANY_NAME));
                }
                return user;
        } catch (SQLException throwable) {
            System.out.println(throwable.getMessage());
            log.error(ConstantsLogger.SQL_EXCEPTION, throwable);
        }
        return null;
    }

    @Override
    public User findByEmail(String email) {
        try (PreparedStatement preparedStatement = connectionProvider.getPreparedStatement(ConstantsDB.FIND_BY_EMAIL)) {
            preparedStatement.setString(1, email);
            ResultSet resultSet = preparedStatement.executeQuery();
            User user = null;
            if (resultSet.next()) {
                user = new User(resultSet.getString(ConstantsUserData.LOGIN),
                        resultSet.getString(ConstantsUserData.PASSWORD),
                        resultSet.getString(ConstantsUserData.EMAIL),
                        resultSet.getString(ConstantsUserData.PHONE_NUMBER),
                        resultSet.getString(ConstantsUserData.COMPANY_NAME));
            }
            return user;
        } catch (SQLException throwable) {
            System.out.println(throwable.getMessage());
            log.error(ConstantsLogger.SQL_EXCEPTION, throwable);
        }
        return null;
    }

    @Override
    public String changePassword(String login) {
        String newPassword = randomPassword.getRandomPassword(login);
        try (PreparedStatement preparedStatement = connectionProvider.getPreparedStatement(ConstantsDB.CHANGE_PASSWORD)) {
            preparedStatement.setString(1, hashPassword.getHash(newPassword));
            preparedStatement.setString(2, login);
                preparedStatement.executeUpdate();
                log.info(ConstantsLogger.USER + login + ConstantsLogger.CHANGE_PASSWORD);
                return newPassword;
            } catch (SQLException throwable) {
                System.out.println(throwable.getMessage());
                log.error(ConstantsLogger.SQL_EXCEPTION, throwable);
            }
        return null;
    }

    @Override
    public Boolean createUser(User user) {
        User foundUser = findByLogin(user.getLogin());
        if (foundUser == null) {
            try (PreparedStatement preparedStatement = connectionProvider.getPreparedStatement(ConstantsDB.INSERT)) {
                preparedStatement.setString(1, user.getLogin());
                preparedStatement.setString(2, hashPassword.getHash(user.getPassword()));
                preparedStatement.setString(3, user.getEmail());
                preparedStatement.setString(4, user.getPhoneNumber());
                preparedStatement.setString(5, user.getCompanyName());

                preparedStatement.executeUpdate();
                log.info(ConstantsLogger.NEW_USER);
                return true;
            } catch (SQLException throwable) {
                System.out.println(throwable.getMessage());
                log.error(ConstantsLogger.SQL_EXCEPTION, throwable);
            }
        }
        return false;
    }

    @Override
    public Boolean updateUser(User user, String login, String password, String email, String phoneNumber, String companyName) {
        User foundUser = findByLogin(user.getLogin());
        if (foundUser != null) {
            try (PreparedStatement preparedStatement = connectionProvider.getPreparedStatement(ConstantsDB.UPDATE)) {
                preparedStatement.setString(1, login);
                preparedStatement.setString(2, hashPassword.getHash(password));
                preparedStatement.setString(3, email);
                preparedStatement.setString(4, phoneNumber);
                preparedStatement.setString(5, companyName);
                preparedStatement.executeUpdate();

                log.info(ConstantsLogger.NEW_USER);
                return true;
            } catch (SQLException throwable) {
                System.out.println(throwable.getMessage());
                log.error(ConstantsLogger.SQL_EXCEPTION, throwable);
            }
        }
        return false;
    }

    @Override
    public String authorization(String login) {
        try {
            if (login != null && connectionProvider.getConnection() != null) {
                PreparedStatement preparedStatementLogin = connectionProvider.getPreparedStatement(ConstantsDB.FIND_BY_LOGIN);
                preparedStatementLogin.setString(1, login);
                ResultSet resultSetLogin = preparedStatementLogin.executeQuery();
                if (resultSetLogin.next()) {
                    return resultSetLogin.getString(ConstantsUserData.PASSWORD);
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            log.error(ConstantsLogger.SQL_EXCEPTION, e);
        }
        return null;
    }

    @Override
    public List<String> getAllUserLogins() {
        List<String> loginList = new LinkedList<>();
        try {
            if (connectionProvider.getConnection() != null) {
                try (Statement statement = connectionProvider.getConnection().createStatement()) {
                    ResultSet resultSet = statement.executeQuery(ConstantsDB.FIND_ALL_LOGINS);
                    while (resultSet.next()) {
                        loginList.add(resultSet.getString(1));
                    }
                } catch (SQLException e) {
                    System.out.println(e.getMessage());
                    log.error(ConstantsLogger.SQL_EXCEPTION, e);
                    return null;
                }
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
            log.error(ConstantsLogger.SQL_EXCEPTION, e);
        }
        return loginList;
    }
}
