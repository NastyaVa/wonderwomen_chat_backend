package models;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

public class ChatsUsers {

    private ConcurrentHashMap<String, List<String>> chatsUsers = new ConcurrentHashMap<>();

    public void addChatAndUsers(String chatName, String... users) {
        chatsUsers.put(chatName, Arrays.asList(users));
    }

    public List<String> getUsersByChatName(String chatName) {
        return chatsUsers.getOrDefault(chatName, null);
    }

    public boolean containsChatName(String chatName) {
        return chatsUsers.containsKey(chatName);
    }
}
