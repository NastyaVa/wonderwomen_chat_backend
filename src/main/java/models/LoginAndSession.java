package models;

import org.eclipse.jetty.websocket.api.Session;
import java.util.Objects;

public class LoginAndSession {

    private String login;
    private Session session;

    public LoginAndSession(String login) {
        this.login = login;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public String getLogin() {
        return login;
    }

    public Session getSession() {
        return session;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        LoginAndSession that = (LoginAndSession) o;
        return Objects.equals(login, that.login) && Objects.equals(session, that.session);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login, session);
    }
}
