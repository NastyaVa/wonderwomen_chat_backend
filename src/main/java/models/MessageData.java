package models;

import utils.enums.Commands;

import java.util.Objects;
import java.util.UUID;

public class MessageData {
    private Message message;
    private UUID uuid;
    private Commands command;

    public MessageData(Message message, UUID uuid, Commands command) {
        this.message = message;
        this.uuid = uuid;
        this.command = command;
    }

    public Message getMessage() {
        return message;
    }

    public Commands getCommand() {
        return command;
    }

    public UUID getUuid() {
        return uuid;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        MessageData that = (MessageData) o;
        return Objects.equals(message, that.message) && Objects.equals(uuid, that.uuid) && Objects.equals(command, that.command);
    }

    @Override
    public int hashCode() {
        return Objects.hash(message, uuid, command);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}