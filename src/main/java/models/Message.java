package models;

import java.util.Objects;

public class Message {

    public String getTo() {
        return to;
    }

    private String from;
    private String to;
    private String payload;
    private long time;

    public Message() {
    }

    public Message(String from, String to, String payload, long time) {
        this.from = from;
        this.to = to;
        this.payload = payload;
        this.time = time;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getPayload() {
        return payload;
    }

    public String getFrom() {
        return from;
    }

    public long getTime() {
        return time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Message message = (Message) o;
        return time == message.time && Objects.equals(from, message.from) && Objects.equals(to, message.to) && Objects.equals(payload, message.payload);
    }

    @Override
    public int hashCode() {
        return Objects.hash(from, to, payload, time);
    }

    @Override
    public String toString() {
        return super.toString();
    }
}
