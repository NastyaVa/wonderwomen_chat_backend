package models;

import java.util.Objects;

public class UserLoginAndIsOnline {
    private final String login;
    private boolean isOnline;

    public UserLoginAndIsOnline(String login) {
        this.login = login;
        this.isOnline = false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        UserLoginAndIsOnline login1 = (UserLoginAndIsOnline) o;
        return Objects.equals(login, login1.login);
    }

    @Override
    public int hashCode() {
        return Objects.hash(login);
    }

    public String getLogin() {
        return login;
    }

    public void setOnline(boolean online) {
        isOnline = online;
    }

    @Override
    public String toString() {
        return "Login{" +
                "login='" + login + '\'' +
                '}';
    }

    public boolean isOnline() {
        return isOnline;
    }
}
