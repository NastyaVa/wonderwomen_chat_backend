package server.services.servlet;
import cache.SessionCache;
import com.google.gson.Gson;
import database.dao.impl.UserDAO;
import models.ResponseData;
import models.User;
import org.json.JSONObject;
import utils.validation.UserDataValidation;
import static javax.servlet.http.HttpServletResponse.SC_FORBIDDEN;
import static org.eclipse.jetty.http.HttpStatus.Code.OK;
import static utils.AppContext.getAppContext;
import static utils.constants.ConstantsAuthRegistration.*;
import static utils.constants.ConstantsUserData.*;
import static utils.constants.ConstantsUserData.LOGIN;
import static utils.constants.ConstantsUserData.PASSWORD;

public class RegistrationService {
    private Gson gson;
    private String jsonInString;
    private int status;
    private ResponseData responseData;
    private UserDAO userDAO;
    private UserDataValidation validationUser;
    private SessionCache sessionCache;

    public RegistrationService(Gson gson, UserDAO userDAO, UserDataValidation validationUser, SessionCache sessionCache) {
        this.gson = gson;
        this.userDAO = userDAO;
        this.validationUser = validationUser;
        this.sessionCache = sessionCache;
    }


    public ResponseData requestProcessing(String requestData) {
        status = SC_FORBIDDEN;
        JSONObject jsonObject = new JSONObject(requestData);
        String login = jsonObject.getString(LOGIN);
        String password = jsonObject.getString(PASSWORD);
        String email = jsonObject.getString(EMAIL);
        String phoneNumber = jsonObject.getString(PHONE_NUMBER);
        String companyName = jsonObject.getString(COMPANY_NAME);

        return fieldsValidationCheck(login, password, email, phoneNumber, companyName);
    }

    private ResponseData fieldsValidationCheck(String login, String password, String email, String phoneNumber, String companyName) {
        if (login != null && password != null && email != null)  {
            if (!getAppContext().getValidationUser().checkLogin(login)) {
                jsonInString = gson.toJson(INCORRECT_LOGIN);
            } else if (!getAppContext().getValidationUser().checkPassword(password)) {
                jsonInString = gson.toJson(INCORRECT_PASSWORD);
            } else if (!getAppContext().getValidationUser().checkEmail(email)) {
                jsonInString = gson.toJson(INCORRECT_EMAIL);
            } else if (!getAppContext().getValidationUser().checkPhoneNumber(phoneNumber)) {
                jsonInString = gson.toJson(INCORRECT_PHONE);
            } else  if (!getAppContext().getValidationUser().checkCompanyName(companyName)) {
                jsonInString = gson.toJson(INCORRECT_COMPANY);
            } else if (getAppContext().getUserDAO().findByLogin(login) == null){
                userDAO.createUser(new User(login, password, email, phoneNumber, companyName));
                status = OK.getCode();
                jsonInString = gson.toJson(REGISTERED);
                responseData = new ResponseData(status, jsonInString);
                return responseData;
            } else {
                jsonInString = gson.toJson(USER_EXISTS);
            }
        } else {
            jsonInString = gson.toJson(FIELDS_NULL);
        }

        responseData = new ResponseData(status, jsonInString);
        return responseData;

}

    /*private void sendLetter () {
        String to = ;
        String from = ;
        String host = "smtp.gmail.com";

        Properties props = new Properties();
        props.put("mail.smtp.host", host);
        props.put("mail.debug", "true");
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.port", "465");
        props.put("mail.smtp.ssl.enable", "true");
        props.put("mail.smtp.socketFactory.class", "javax.net.ssl.SSLSocketFactory");
        Session session = Session.getInstance(props);

        try {

            Transport bus = session.getTransport("smtp");
            bus.connect("smtp.gmail.com", "", "");
            Message msg = new MimeMessage(session);

            msg.setFrom(new InternetAddress(from));
            InternetAddress[] address = {new InternetAddress(to)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setRecipients(Message.RecipientType.CC,
                    InternetAddress.parse(to, true));
            msg.setSubject("Hello, world");
            msg.setSentDate(new Date());
            String mytxt = "O-la-la-la-la-la";
            msg.setText(mytxt);
            msg.saveChanges();
            bus.sendMessage(msg, address);
            bus.close();
        }
        catch (MessagingException mex) {
            mex.printStackTrace();
            while (mex.getNextException() != null) {
                Exception ex = mex.getNextException();
                ex.printStackTrace();
                if (!(ex instanceof MessagingException)) break;
                else mex = (MessagingException)ex;
            }
       }
    }*/


   /* public class EmailAuthenticator extends Authenticator {
        private String login   ;
        private String password;
        public EmailAuthenticator(String email, String psw) {
            this.login    = email;
            this.password = psw;
        }
        public PasswordAuthentication getPasswordAuthentication()
        {
            return new PasswordAuthentication(login, password);
        }
    }*/

}
