package server.services.servlet;

import cache.SessionCache;
import com.google.gson.Gson;
import database.dao.impl.UserDAO;
import database.subsidiary.HashPassword;
import models.ResponseData;
import org.json.JSONObject;
import utils.validation.UserDataValidation;
import java.util.UUID;
import static javax.servlet.http.HttpServletResponse.SC_FORBIDDEN;
import static org.eclipse.jetty.http.HttpStatus.Code.OK;
import static utils.constants.ConstantsAuthRegistration.*;

public class AuthorizationService {

    private Gson gson;
    private String jsonInString;
    private int status;
    private ResponseData authResponseData;
    private UserDAO userDAO;
    private UserDataValidation validationUser;
    private SessionCache sessionCache;
    private HashPassword hashPassword;

    public AuthorizationService(Gson gson, UserDAO userDAO, UserDataValidation validationUser, SessionCache sessionCache, HashPassword hashPassword) {
        this.gson = gson;
        this.userDAO = userDAO;
        this.validationUser = validationUser;
        this.sessionCache = sessionCache;
        this.hashPassword = hashPassword;
    }


    public ResponseData requestProcessing(String requestData) {
        status = SC_FORBIDDEN;
        JSONObject jsonObject = new JSONObject(requestData);
        String login = jsonObject.getString(LOGIN);
        String password = jsonObject.getString(PASSWORD);

        return loginAndPasswordValidationCheck(login, password);
    }

    private ResponseData loginAndPasswordValidationCheck(String login, String password) {
        if (!validationUser.checkLogin(login)) {
            jsonInString = gson.toJson(INCORRECT_LOGIN);
        } else if (!validationUser.checkPassword(password)) {
            jsonInString = gson.toJson(INCORRECT_PASSWORD);
        } else if (userDAO.findByLogin(login) != null) {
            if (hashPassword.getHash(password).equals(userDAO.authorization(login))) {
                    UUID uuid = sessionCache.addCache(login);
                    status = OK.getCode();
                    jsonInString = gson.toJson(uuid);
            } else {
                jsonInString = gson.toJson(LOGIN_OR_PASS_NOT_FOUND);
            }
        } else {
            jsonInString = gson.toJson(LOGIN_OR_PASS_NOT_FOUND);
        }
        authResponseData = new ResponseData(status, jsonInString);
        return authResponseData;
    }
}
