package server.services.servlet;

import cache.SessionCache;
import database.dao.impl.UserDAO;
import models.LoginAndSession;
import models.UserLoginAndIsOnline;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class ChatUsersGetterService {

    SessionCache sessionCache;
    UserDAO userDAO;

    public ChatUsersGetterService(SessionCache sessionCache, UserDAO userDAO) {
        this.sessionCache = sessionCache;
        this.userDAO = userDAO;
    }

    public List<UserLoginAndIsOnline> getUsersList() {
        List<UserLoginAndIsOnline> onlineUsers = new ArrayList<>();

        ConcurrentHashMap<UUID, LoginAndSession> cache = sessionCache.getCache();

        List<String> logins = userDAO.getAllUserLogins();

        logins.forEach(al -> onlineUsers.add(new UserLoginAndIsOnline(al)));

        Iterator<Map.Entry<UUID, LoginAndSession>> iterator = cache.entrySet().iterator();
        while (iterator.hasNext()) {
            Map.Entry<UUID, LoginAndSession> pair = iterator.next();
            if (pair.getValue().getSession() != null) {
                onlineUsers.forEach(userLoginAndIsOnline -> {
                    if (pair.getValue().getLogin().equals(userLoginAndIsOnline.getLogin())) {
                        userLoginAndIsOnline.setOnline(true);
                    }
                });
            }
        }

        return onlineUsers;
    }
}
