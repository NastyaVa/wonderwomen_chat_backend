package server.services.servlet;

import com.google.gson.Gson;
import database.dao.impl.UserDAO;
import models.ResponseData;
import org.json.JSONObject;
import static javax.servlet.http.HttpServletResponse.SC_FORBIDDEN;
import static org.eclipse.jetty.http.HttpStatus.Code.OK;
import static utils.AppContext.getAppContext;
import static utils.constants.ConstantsAuthRegistration.*;
import static utils.constants.ConstantsUserData.*;
import static utils.constants.ConstantsUserData.LOGIN;

public class ForgotPasswordService {
    private final Gson gson;
    private String jsonInString;
    private int status;
    private ResponseData responseData;
    private final UserDAO userDAO;


    public ForgotPasswordService(Gson gson, UserDAO userDAO) {
        this.gson = gson;
        this.userDAO = userDAO;
    }


    public ResponseData requestProcessing(String requestData) {
        status = SC_FORBIDDEN;
        JSONObject jsonObject = new JSONObject(requestData);
        String login = jsonObject.getString(LOGIN);
        String email = jsonObject.getString(EMAIL);

        return fieldsValidationCheck(login, email);
    }

    private ResponseData fieldsValidationCheck(String login, String email) {
        if (login != null  && email != null)  {
            if (!getAppContext().getValidationUser().checkLogin(login)) {
                jsonInString = gson.toJson(INCORRECT_LOGIN);
            } else if (!getAppContext().getValidationUser().checkEmail(email)) {
                jsonInString = gson.toJson(INCORRECT_EMAIL);
            } else {
                if (userDAO.findByLogin(login) != null && userDAO.findByEmail(email) != null) {
                    jsonInString = gson.toJson(CHANGED_PSW + " " + userDAO.changePassword(login) + ".");
                    status = OK.getCode();
                    responseData = new ResponseData(status, jsonInString);
                    return responseData;
                } else {
                    jsonInString = gson.toJson(USER_NOT_FOUND);
                }
            }
        } else {
            jsonInString = gson.toJson(FIELDS_NULL_L_E);
        }
        responseData = new ResponseData(status, jsonInString);
        System.out.println(responseData.getMessage());
        return responseData;

    }

}
