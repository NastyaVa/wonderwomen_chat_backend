package server.services.websocket;

import org.apache.log4j.Logger;
import cache.MessagePool;
import cache.SessionCache;
import models.Message;
import models.MessageData;
import org.eclipse.jetty.websocket.api.Session;
import org.json.JSONObject;
import utils.enums.Commands;
import utils.validation.MessageValidation;
import java.io.IOException;
import java.util.Calendar;
import java.util.UUID;
import static utils.constants.ConstantsWebSocket.*;
import static utils.constants.ConstantsMessage.*;
import static utils.constants.ConstantsWebSocket.GENERAL_CHAT;
import static utils.enums.Commands.*;

public class WebsocketRequestsProcessingService {

    private static final Logger log = Logger.getLogger(WebsocketRequestsProcessingService.class);
    SessionCache sessionCache;
    MessagePool messagePool;
    MessageValidation messageValidation;

    public WebsocketRequestsProcessingService(SessionCache sessionCache, MessagePool messagePool,
                                              MessageValidation messageValidation) {
        this.sessionCache = sessionCache;
        this.messagePool = messagePool;
        this.messageValidation = messageValidation;
    }

    public void onTextService(Session session, String message) throws IOException {
        UUID uuid = sessionCache.getUUIDBySession(session);
        if (uuid == null) {
            session.disconnect();
            return;
        }
        if (message.equals(PING)) {
            log.info(PING_PLUS_PONG);
        } else {
            JSONObject jsonObject = new JSONObject(message);
            if (!messageValidation.validateMessageContent(jsonObject)) {
                addSystemMessage(MESSAGE_CONTENT_INCORRECT, uuid, INCORRECT_MESSAGE);
            } else if (!messageValidation.validateMessageLength(jsonObject)) {
                addSystemMessage(MESSAGE_LENGTH_UNACCEPTABLE, uuid, INCORRECT_MESSAGE);
            } else {
                String to = String.valueOf(jsonObject.get(TO));
                String payload = String.valueOf(jsonObject.get(PAYLOAD));
                long time = Long.parseLong(String.valueOf(jsonObject.get(TIME)));
                String from = sessionCache.getCache().get(uuid).getLogin();
                Commands command = Commands.valueOf((String) jsonObject.get(COMMAND));
                Message messageModel = new Message(from, to, payload, time);
                addMessage(messageModel, uuid, command);
            }
        }
    }

    public void onConnectService(Session session) throws IOException {
        UUID uuid = sessionCache.setSessionAndUUID(session);
        if (uuid == null) {
            session.disconnect();
            return;
        }
        if (!sessionCache.isUserLoggedInMoreThanOnce(uuid)) {
            addSystemMessage(USER_JOINED_THE_CHAT, uuid, NEW_USER_CONNECTED);
        }
        addSystemMessage(null, uuid, CHAT_USERS_GETTER);
        addSystemMessage(null, uuid, GET_USER_CHATS);
        addSystemMessage(null, uuid, GET_CHAT_HISTORY);
        log.info(session.getRemoteAddress().getHostString() + "connected!");
    }

    public void onCloseService(Session session) {
        UUID uuid = sessionCache.getUUIDBySession(session);
        addSystemMessage(USER_LEFT_THE_CHAT, uuid, USER_DISCONNECTED);
        log.info(session.getRemoteAddress().getHostString() + "closed!");
    }

    private void addSystemMessage(String payload, UUID uuid, Commands command) {
        Message messageModel = new Message();
        messageModel.setTime(Calendar.getInstance().getTimeInMillis());
        messageModel.setFrom(SERVER);
        if (command == GET_CHAT_HISTORY) {
            messageModel.setTo(GENERAL_CHAT);
        }
        if (payload != null) {
            messageModel.setPayload(payload);
        }
        addMessage(messageModel, uuid, command);
    }

    private void addMessage(Message messageModel, UUID uuid, Commands command) {
        MessageData messageData = new MessageData(messageModel, uuid, command);
        messagePool.addMessage(messageData);
    }
}
