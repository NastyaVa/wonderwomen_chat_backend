package server.services.websocket.command_services.impl;

import cache.SessionCache;
import com.google.gson.Gson;
import models.LoginAndSession;
import models.Message;
import models.UserPC;
import org.apache.log4j.Logger;
import org.eclipse.jetty.websocket.api.Session;
import server.services.websocket.command_services.ICommandService;
import server.services.websocket.subsidiary.ChatUsersGetter;
import utils.AppContext;
import utils.enums.Commands;
import java.io.IOException;
import java.util.*;

public class ChatUsersGetterService implements ICommandService {

    private static final Logger log = Logger.getLogger(ChatUsersGetterService.class);
    Gson gson;
    ChatUsersGetter chatUsersGetter;
    SessionCache sessionCache;

    public ChatUsersGetterService(Gson gson, ChatUsersGetter chatUsersGetter, SessionCache sessionCache) {
        this.gson = gson;
        this.chatUsersGetter = chatUsersGetter;
        this.sessionCache = sessionCache;
    }

    @Override
    public boolean sendMessage(Message message, UUID currUserUUID) {
        LoginAndSession loginAndSession = sessionCache.findByUUID(currUserUUID);
        if (loginAndSession != null) {
            if (loginAndSession.getSession() != null) {
                Session currSession = sessionCache.findByUUID(currUserUUID).getSession();
                message.setPayload(gson.toJson(chatUsersGetter.getUsersList()));
                UserPC userPC = new UserPC(Commands.CHAT_USERS_GETTER.toString(), message);
                try {
                    currSession.getRemote().sendString(gson.toJson(userPC));
                    return true;
                } catch (IOException e) {
                    log.info(e.getMessage());
                }
            }
        }
        return false;
    }
}
