package server.services.websocket.command_services.impl;

import cache.SessionCache;
import com.google.gson.Gson;
import models.Message;
import models.UserPC;
import org.apache.log4j.Logger;
import org.eclipse.jetty.websocket.api.Session;
import server.services.websocket.command_services.ICommandService;

import java.io.IOException;
import java.util.UUID;

import static utils.enums.Commands.INCORRECT_MESSAGE;

public class IncorrectMessageService implements ICommandService {

    Gson gson;
    SessionCache sessionCache;
    private static final Logger log = Logger.getLogger(IncorrectMessageService.class);

    public IncorrectMessageService(Gson gson, SessionCache sessionCache) {
        this.gson = gson;
        this.sessionCache = sessionCache;
    }

    @Override
    public boolean sendMessage(Message message, UUID currUserUUID) {
        Session currSession = sessionCache.getCache().get(currUserUUID).getSession();
        UserPC userPC = new UserPC(INCORRECT_MESSAGE.toString(), message);
        try {
            currSession.getRemote().sendString(gson.toJson(userPC));
        } catch (IOException e) {
            log.info(e.getMessage());
            return true;
        }
        return false;
    }
}
