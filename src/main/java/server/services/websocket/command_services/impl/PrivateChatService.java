package server.services.websocket.command_services.impl;

import cache.SessionCache;
import com.google.gson.Gson;
import database.dao.ChatsHistoryDAO;
import models.ChatsUsers;
import models.LoginAndSession;
import models.Message;
import models.UserPC;
import org.apache.log4j.Logger;
import server.services.websocket.command_services.ICommandService;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import static utils.enums.Commands.PRIVATE_CHAT;

public class PrivateChatService implements ICommandService {

    private static final Logger log = Logger.getLogger(GeneralChatService.class);
    Gson gson;
    SessionCache sessionCache;
    ChatsHistoryDAO chatsHistoryDAO;
    ChatsUsers chatsUsers;

    public PrivateChatService(Gson gson, SessionCache sessionCache, ChatsHistoryDAO chatsHistoryDAO, ChatsUsers chatsUsers) {
        this.gson = gson;
        this.sessionCache = sessionCache;
        this.chatsHistoryDAO = chatsHistoryDAO;
        this.chatsUsers = chatsUsers;
    }

    @Override
    public boolean sendMessage(Message message, UUID currUserUUID) {
        chatsHistoryDAO.saveMessages(message);
        if (!sessionCache.getCache().isEmpty()) {
            UserPC userPC = new UserPC(PRIVATE_CHAT.toString(), message);
            List<String> chatUsers = chatsUsers.getUsersByChatName(message.getTo());
            for (LoginAndSession value : sessionCache.getCache().values()) {
                for (String s : chatUsers) {
                    if (value.getLogin().equals(s)) {
                        try {
                            value.getSession().getRemote().sendString(gson.toJson(userPC));
                        } catch (IOException e) {
                            log.error(e.getMessage());
                        }
                    }
                }
            }
       }
        return false;
    }
}
