package server.services.websocket.command_services.impl;

import cache.SessionCache;
import com.google.gson.Gson;
import models.LoginAndSession;
import models.Message;
import models.UserPC;
import org.apache.log4j.Logger;
import org.eclipse.jetty.websocket.api.Session;
import server.services.websocket.command_services.ICommandService;
import java.io.IOException;
import java.util.UUID;
import static utils.constants.ConstantsWebSocket.USER_JOINED_THE_CHAT;
import static utils.constants.ConstantsWebSocket.USER_LEFT_THE_CHAT;
import static utils.enums.Commands.NEW_USER_CONNECTED;
import static utils.enums.Commands.USER_DISCONNECTED;

public class UserConnAndDisconnService implements ICommandService {

    private static final Logger log = Logger.getLogger(UserConnAndDisconnService.class);
    Gson gson;
    SessionCache sessionCache;

    public UserConnAndDisconnService(Gson gson, SessionCache sessionCache) {
        this.gson = gson;
        this.sessionCache = sessionCache;
    }

    @Override
    public boolean sendMessage(Message message, UUID currUserUUID) {
        boolean returnValue = false;
        if (sessionCache.getCache().isEmpty()) {
            return false;
        }
        String command;
        if (message.getPayload().equals(USER_JOINED_THE_CHAT)) {
            command = NEW_USER_CONNECTED.toString();
        } else {
            command = USER_DISCONNECTED.toString();
        }
        if ((command.equals(NEW_USER_CONNECTED.toString())) || (!sessionCache.isUserLoggedInMoreThanOnce(currUserUUID))) {

            for (UUID key : sessionCache.getCache().keySet()) {
                try {
                    Session currSession = sessionCache.getCache().get(key).getSession();
                    LoginAndSession loginAndSession = sessionCache.getCache().get(currUserUUID);
                    if (loginAndSession == null) {
                        return false;
                    }
                        message.setFrom(loginAndSession.getLogin());
                        UserPC userPC;
                        userPC = new UserPC(command, message);
                        if (loginAndSession.getSession() == null) {
                            return false;
                        }
                        if (!currSession.equals(loginAndSession.getSession())) {
                            currSession.getRemote().sendString(gson.toJson(userPC));
                            returnValue = true;
                        }
                } catch (IOException e) {
                    log.info(e.getMessage());
                }
            }
        }
        if (message.getPayload().equals(USER_LEFT_THE_CHAT)) {
            sessionCache.removeByUUID(currUserUUID);
        }
        return returnValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        } else if (getClass() == o.getClass()) {
            return true;
        }
        return false;
    }
}
