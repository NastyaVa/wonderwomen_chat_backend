package server.services.websocket.command_services.impl;

import models.Message;
import server.services.websocket.command_services.ICommandService;
import java.util.UUID;

public class DefaultService implements ICommandService {

    @Override
    public boolean sendMessage(Message message, UUID currUserUUID) {
        return true;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        } else if (getClass() == o.getClass()) {
            return true;
        }
        return false;
    }
}
