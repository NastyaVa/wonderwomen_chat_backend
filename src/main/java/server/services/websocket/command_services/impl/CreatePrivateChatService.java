package server.services.websocket.command_services.impl;

import cache.SessionCache;
import com.google.gson.Gson;
import database.dao.PrivateChatsDAO;
import models.ChatsUsers;
import models.LoginAndSession;
import models.Message;
import models.UserPC;
import org.apache.log4j.Logger;
import server.services.websocket.command_services.ICommandService;
import utils.validation.ChatNameValidation;
import java.io.IOException;
import java.util.Arrays;
import java.util.UUID;

import static utils.constants.ConstantsDB.*;
import static utils.enums.Commands.CREATE_PRIVATE_CHAT;

public class CreatePrivateChatService implements ICommandService {

    private static final Logger log = Logger.getLogger(GeneralChatService.class);
    PrivateChatsDAO createPrivateChat;
    SessionCache sessionCache;
    Gson gson;
    ChatNameValidation chatNameValidation;
    ChatsUsers chatsUsers;

    public CreatePrivateChatService(PrivateChatsDAO createPrivateChat, SessionCache sessionCache, Gson gson,
                                    ChatNameValidation chatNameValidation, ChatsUsers chatsUsers) {
        this.createPrivateChat = createPrivateChat;
        this.sessionCache = sessionCache;
        this.gson = gson;
        this.chatNameValidation = chatNameValidation;
        this.chatsUsers = chatsUsers;
    }

    @Override
    public boolean sendMessage(Message message, UUID currUserUUID) {
        String logins = message.getPayload();
        String chatName = message.getTo();
        String[] loginsArray = logins.split(SEMICOLON);
        int length = loginsArray.length;
        String loginCreator = loginsArray[length - 1];

        String tableName = String.format(TABLE_NAME, chatName, loginCreator);

        if (!chatNameValidation.validateChatName(chatName)) {
            sendErrorMessage(CHAT_NAME_INCORRECT, message, currUserUUID);
            return false;
        }

        if (createPrivateChat.checkIfChatNameExists(tableName)) {
            sendErrorMessage(CHAT_NAME_ALREADY_EXISTS, message, currUserUUID);
            return false;
        }

        message.setTo(tableName);
        UserPC userPC = new UserPC(CREATE_PRIVATE_CHAT.toString(), message);
        if (createPrivateChat.addLoginsAndChatName(tableName, loginsArray)) {
            chatsUsers.addChatAndUsers(tableName, loginsArray);
            for (LoginAndSession value : sessionCache.getCache().values()) {
                for (String s : loginsArray) {
                    if (value.getLogin().equals(s)) {
                        try {
                            value.getSession().getRemote().sendString(gson.toJson(userPC));
                        } catch (IOException e) {
                            log.error(e.getMessage());
                        }
                    }
                }
            }
            return true;
        }

        return false;
    }

    private void sendErrorMessage(String payload, Message message, UUID currUserUUID) {
        message.setPayload(payload);
        UserPC userPC = new UserPC(CREATE_PRIVATE_CHAT.toString(), message);
        try {
            sessionCache.findByUUID(currUserUUID).getSession().getRemote().sendString(gson.toJson(userPC));
        } catch (IOException e) {
            log.error(e.getMessage());
        }
    }
}
