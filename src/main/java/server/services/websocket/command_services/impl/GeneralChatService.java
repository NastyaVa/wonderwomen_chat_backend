package server.services.websocket.command_services.impl;

import cache.SessionCache;
import com.google.gson.Gson;
import database.dao.ChatsHistoryDAO;
import models.Message;
import models.UserPC;
import org.apache.log4j.Logger;
import server.services.websocket.command_services.ICommandService;
import utils.enums.Commands;
import java.io.IOException;
import java.util.UUID;

public class GeneralChatService implements ICommandService {

    private static final Logger log = Logger.getLogger(GeneralChatService.class);
    Gson gson;
    SessionCache sessionCache;
    ChatsHistoryDAO chatsHistoryDAO;

    public GeneralChatService(Gson gson, SessionCache sessionCache, ChatsHistoryDAO chatsHistoryDAO) {
        this.gson = gson;
        this.sessionCache = sessionCache;
        this.chatsHistoryDAO = chatsHistoryDAO;
    }

    @Override
    public boolean sendMessage(Message message, UUID currUserUUID) {
        chatsHistoryDAO.saveMessages(message);
        if (!sessionCache.getCache().isEmpty()) {
            UserPC userPC = new UserPC(Commands.GENERAL_CHAT.toString(), message);
            for (UUID key : sessionCache.getCache().keySet()) {
                try {
                    if (sessionCache.getCache().get(key).getSession() != null) {
                        sessionCache.getCache().get(key).getSession().getRemote().sendString(gson.toJson(userPC));
                    } else {
                        return false;
                    }
                } catch (IOException e) {
                    log.info(e.getMessage());
                }
            }
            return true;
        }
        return false;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null) {
            return false;
        } else if (getClass() == o.getClass()) {
            return true;
        }
        return false;
    }
}
