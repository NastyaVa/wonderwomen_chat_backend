package server.services.websocket.command_services.impl;

import cache.SessionCache;
import com.google.gson.Gson;
import database.dao.ChatsHistoryDAO;
import models.Message;
import models.UserPC;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.eclipse.jetty.websocket.api.Session;
import server.services.websocket.command_services.ICommandService;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import static utils.enums.Commands.GET_USER_CHATS;

public class GetUserChats implements ICommandService {

    ChatsHistoryDAO chatsHistoryDAO;
    Gson gson;
    SessionCache sessionCache;
    private final Logger logger = LogManager.getLogger(GetUserChats.class);

    public GetUserChats(ChatsHistoryDAO chatsHistoryDAO, Gson gson, SessionCache sessionCache) {
        this.chatsHistoryDAO = chatsHistoryDAO;
        this.gson = gson;
        this.sessionCache = sessionCache;
    }

    @Override
    public boolean sendMessage(Message message, UUID currUserUUID) {
        String login = sessionCache.getCache().get(currUserUUID).getLogin();
        if (sessionCache.getCache().isEmpty() || login.length() == 0) {
            return false;
        }

        List<String> chats = chatsHistoryDAO.getChatsByLogin(login);
        if (chats.size() == 0) {
            return false;
        }
        message.setPayload(gson.toJson(chats));
        if (sessionCache.getCache().get(currUserUUID) == null) {
            return false;
        }
        Session currSession = sessionCache.getCache().get(currUserUUID).getSession();

        UserPC userPC = new UserPC(GET_USER_CHATS.toString(), message);
        try {
            currSession.getRemote().sendString(gson.toJson(userPC));
            return true;
        } catch (IOException e) {
            logger.info(e.getMessage());
            return false;
        }
    }
}
