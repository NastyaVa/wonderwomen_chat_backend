package server.services.websocket.command_services.impl;

import cache.SessionCache;
import com.google.gson.Gson;
import database.dao.ChatsHistoryDAO;
import models.ChatsUsers;
import models.Message;
import models.UserPC;
import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;
import org.eclipse.jetty.websocket.api.Session;
import server.services.websocket.command_services.ICommandService;
import java.io.IOException;
import java.util.List;
import java.util.Queue;
import java.util.UUID;
import static utils.constants.ConstantsWebSocket.SERVER;
import static utils.enums.Commands.GET_CHAT_HISTORY;

public class GetChatsHistory implements ICommandService {

    ChatsHistoryDAO chatsHistoryDAO;
    Gson gson;
    SessionCache sessionCache;
    ChatsUsers chatsUsers;
    private final Logger logger = LogManager.getLogger(GetChatsHistory.class);

    public GetChatsHistory(ChatsHistoryDAO chatsHistoryDAO, Gson gson, SessionCache sessionCache, ChatsUsers chatsUsers) {
        this.chatsHistoryDAO = chatsHistoryDAO;
        this.gson = gson;
        this.sessionCache = sessionCache;
        this.chatsUsers = chatsUsers;
    }

    @Override
    public boolean sendMessage(Message message, UUID currUserUUID) {
        if (sessionCache.getCache().isEmpty()) {
            return false;
        }
        if (sessionCache.getCache().get(currUserUUID) == null) {
            return false;
        }
        String chatName = message.getTo();
        addChatAndUsersToCache(chatName);
        Session currSession = sessionCache.getCache().get(currUserUUID).getSession();
        Queue<Message> messages = chatsHistoryDAO.getListMessages(message.getTo());
        message.setPayload(gson.toJson(messages));
        message.setFrom(SERVER);
        UserPC userPC = new UserPC(GET_CHAT_HISTORY.toString(), message);
        try {
            currSession.getRemote().sendString(gson.toJson(userPC));
            return true;
        } catch (IOException e) {
            logger.info(e.getMessage());
            return false;
        }
    }

    private boolean addChatAndUsersToCache(String chatName) {
        if (chatsUsers.containsChatName(chatName)) {
            return false;
        }
        List<String> logins = chatsHistoryDAO.getLoginsByChatName(chatName);
        chatsUsers.addChatAndUsers(chatName, logins.toArray(new String[0]));
        return true;
    }

}
