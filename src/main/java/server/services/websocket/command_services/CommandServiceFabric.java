package server.services.websocket.command_services;

import cache.SessionCache;
import com.google.gson.Gson;
import database.dao.ChatsHistoryDAO;
import database.dao.PrivateChatsDAO;
import models.ChatsUsers;
import server.services.websocket.command_services.impl.*;
import server.services.websocket.subsidiary.ChatUsersGetter;
import utils.enums.Commands;
import utils.validation.ChatNameValidation;

public class CommandServiceFabric {

    Gson gson;
    SessionCache sessionCache;
    ChatUsersGetter chatUsersGetter;
    ChatsHistoryDAO chatsHistoryDAO;
    PrivateChatsDAO createPrivateChat;
    ChatNameValidation chatNameValidation;
    ChatsUsers chatsUsers;

    public CommandServiceFabric(Gson gson, SessionCache sessionCache, ChatUsersGetter chatUsersGetter, ChatsHistoryDAO chatsHistoryDAO,
                                PrivateChatsDAO createPrivateChat, ChatNameValidation chatNameValidation, ChatsUsers chatsUsers) {
        this.gson = gson;
        this.sessionCache = sessionCache;
        this.chatUsersGetter = chatUsersGetter;
        this.chatsHistoryDAO = chatsHistoryDAO;
        this.createPrivateChat = createPrivateChat;
        this.chatNameValidation = chatNameValidation;
        this.chatsUsers = chatsUsers;
    }

    public ICommandService chooseSession(Commands commands) {
        ICommandService instance;

        switch (commands) {
            case GENERAL_CHAT:
                instance = new GeneralChatService(gson, sessionCache, chatsHistoryDAO);
                break;
            case PRIVATE_CHAT:
                instance = new PrivateChatService(gson, sessionCache, chatsHistoryDAO, chatsUsers);
                break;
            case NEW_USER_CONNECTED:
            case USER_DISCONNECTED:
                instance = new UserConnAndDisconnService(gson, sessionCache);
                break;
            case INCORRECT_MESSAGE:
                instance = new IncorrectMessageService(gson, sessionCache);
                break;
            case CHAT_USERS_GETTER:
                instance = new ChatUsersGetterService(gson, chatUsersGetter, sessionCache);
                break;
            case GET_USER_CHATS:
                instance = new GetUserChats(chatsHistoryDAO, gson, sessionCache);
                break;
            case GET_CHAT_HISTORY:
                instance = new GetChatsHistory(chatsHistoryDAO, gson, sessionCache, chatsUsers);
                break;
            case CREATE_PRIVATE_CHAT:
                instance = new CreatePrivateChatService(createPrivateChat, sessionCache, gson, chatNameValidation, chatsUsers);
                break;
            default:
                instance = new DefaultService();
        }
        return instance;
    }
}
