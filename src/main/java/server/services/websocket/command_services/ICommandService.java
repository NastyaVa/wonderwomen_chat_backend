package server.services.websocket.command_services;

import models.LoginAndSession;
import models.Message;
import utils.AppContext;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public interface ICommandService {

    boolean sendMessage(Message message, UUID currUserUUID);

}
