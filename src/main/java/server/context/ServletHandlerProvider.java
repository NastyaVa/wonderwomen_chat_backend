package server.context;

import org.eclipse.jetty.servlet.ServletHandler;
import server.endpoints.AuthorizationServlet;
import server.endpoints.ForgotPasswordServlet;
import server.endpoints.RegistrationServlet;
import javax.servlet.DispatcherType;
import java.util.EnumSet;
import static utils.AppContext.getAppContext;

public class ServletHandlerProvider {

    public static ServletHandler getServletHandler() {

        ServletHandler handler = new ServletHandler();
        handler.addServletWithMapping(AuthorizationServlet.class, getAppContext().getProperty().getLoginMapping());
        handler.addServletWithMapping(RegistrationServlet.class, getAppContext().getProperty().getRegistrationMapping());
        handler.addServletWithMapping(ForgotPasswordServlet.class, getAppContext().getProperty().getForgotPasswordMapping());
        handler.addFilterWithMapping(CORSFilter.class, "/*", EnumSet.of(DispatcherType.REQUEST));
        return handler;

    }

}