package server.endpoints;

import models.ResponseData;
import server.services.servlet.ForgotPasswordService;
import utils.AppContext;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

@WebServlet("/forgotPassword")
public class ForgotPasswordServlet extends HttpServlet {

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        String requestData = req.getReader().lines().collect(Collectors.joining());
        ResponseData responseData = AppContext.getAppContext().getForgotPasswordService().requestProcessing(requestData);

        resp.setStatus(responseData.getCode());
        resp.getWriter().println(responseData.getMessage());
    }
}
