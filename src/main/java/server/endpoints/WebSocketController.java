package server.endpoints;

import org.apache.log4j.Logger;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketClose;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketConnect;
import org.eclipse.jetty.websocket.api.annotations.OnWebSocketMessage;
import org.eclipse.jetty.websocket.api.annotations.WebSocket;
import server.services.websocket.WebsocketRequestsProcessingService;

import java.io.IOException;
import static utils.AppContext.getAppContext;

@WebSocket
public class WebSocketController {

    private static final Logger log = Logger.getLogger(WebSocketController.class);

    @OnWebSocketMessage
    public void onText(Session session, String message) throws IOException {
        log.info("Message received:" + message);
        if (session.isOpen()) {
            getAppContext().getWebsocketRequestsProcessingService().onTextService(session, message);
        }
    }

    @OnWebSocketConnect
    public void onConnect(Session session) throws IOException {
        getAppContext().getWebsocketRequestsProcessingService().onConnectService(session);
    }

    @OnWebSocketClose
    public void onClose(Session session, int status, String reason) {
        getAppContext().getWebsocketRequestsProcessingService().onCloseService(session);
    }

}
