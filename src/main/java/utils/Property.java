package utils;

import utils.constants.ConstantsUserData;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class Property {

    private InputStream fis;
    private Properties property;

    public Property() {
        property = new Properties();
        fis = Property.class.getClassLoader().getResourceAsStream(ConstantsUserData.NAME_PROPERTY_FILE);
        try {
            property.load(fis);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getHost() {
        return property.getProperty("db.host");
    }

    public String getLogin() {
        return property.getProperty("db.login");
    }

    public String getPassword() {
        return property.getProperty("db.password");
    }

    public String getDriver() {
        return property.getProperty("db.driver");
    }

    public String getLoginMapping() {
        return property.getProperty("server.mapping.login");
    }

    public String getChatMapping() {
        return property.getProperty("server.mapping.chat");
    }

    public String getRegistrationMapping() {
        return property.getProperty("server.mapping.registration");
    }

    public String getForgotPasswordMapping() {
        return property.getProperty("server.mapping.forgot.password");
    }

    public String getPort() {
        return property.getProperty("server.port");
    }

    public String getSalt() {
        return property.getProperty("salt");
    }

}
