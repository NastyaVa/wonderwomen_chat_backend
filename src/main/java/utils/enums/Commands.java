package utils.enums;

public enum Commands {

    GENERAL_CHAT,
    PRIVATE_CHAT,
    DEFAULT_COMMAND,
    NEW_USER_CONNECTED,
    USER_DISCONNECTED,
    INCORRECT_MESSAGE,
    GET_USER_CHATS,
    CHAT_USERS_GETTER,
    GET_CHAT_HISTORY,
    CREATE_PRIVATE_CHAT;

    private String command;


    Commands() {

    }

    public String getCommand() {
        return command;
    }
}
