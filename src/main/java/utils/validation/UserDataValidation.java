package utils.validation;
import java.util.regex.Pattern;

public class UserDataValidation {

    public Boolean checkLogin(String login) {
        return Pattern.compile("[A-Za-z0-9]{4,15}").matcher(login).matches();
    }

    public Boolean checkPassword(String password) {
        return Pattern.compile("[A-Za-z0-9#?!@$%^&*-]{8,15}").matcher(password).matches();
    }

    public Boolean checkEmail(String email) {
        String ePattern = "^[a-z0-9.]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-z\\-0-9]+\\.)+[a-z]{2,}))$";
        return Pattern.compile(ePattern).matcher(email).matches();
    }

    public Boolean checkPhoneNumber(String phoneNumber) {
        String ePattern = "^(\\s*)?(\\+)?([- _():+]?\\d[- _():+]?){10,14}(\\s*)?$";
        return Pattern.compile(ePattern).matcher(phoneNumber).matches() || phoneNumber.isEmpty();
    }

    public Boolean checkCompanyName(String companyName) {
        return Pattern.compile("[A-Za-z0-9 -]{3,15}").matcher(companyName).matches() || companyName.isEmpty();
    }
}
