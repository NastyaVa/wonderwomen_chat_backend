package utils.validation;

import org.json.JSONObject;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import static utils.constants.ConstantsMessage.*;

public class MessageValidation {

    public boolean validateMessageContent(JSONObject message) {
        Set<String> keySet = message.keySet();
        Set<String> requiredKeySet = new HashSet<>(Arrays.asList(PAYLOAD, TIME, TO, COMMAND));
        return keySet.containsAll(requiredKeySet);
    }

    public boolean validateMessageLength(JSONObject message) {
        int msgLength = String.valueOf(message.get(PAYLOAD)).length();
        return msgLength <= 300 && msgLength >= 1;
    }
}
