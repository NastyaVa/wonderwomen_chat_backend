package utils.validation;

import java.util.regex.Pattern;

public class ChatNameValidation {

    public boolean validateChatName(String chatName) {
        return Pattern.compile("[A-Za-z0-9]{4,15}").matcher(chatName).matches();
    }
}
