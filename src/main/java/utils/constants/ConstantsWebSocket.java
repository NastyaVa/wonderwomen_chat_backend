package utils.constants;

public class ConstantsWebSocket {

    public static final String USER_JOINED_THE_CHAT = "joined the chat :)";
    public static final String USER_LEFT_THE_CHAT = "left the chat :(";
    public static final String MESSAGE_CONTENT_INCORRECT = "Message content is incorrect";
    public static final String MESSAGE_LENGTH_UNACCEPTABLE = "Message length should be from 1 to 300 characters long.";
    public static final String GENERAL_CHAT = "general_chat";
    public static final String SERVER = "Server";
    public static final String PING = "PING";
    public static final String PING_PLUS_PONG = "PING + PONG";
}
