package utils.constants;

public class ConstantsLogger {

    public static final String SQL_EXCEPTION = "SQLException";
    public static final String EXCEPTION = "Exception";
    public static final String USER = "User ";
    public static final String LOGGED_IN = " is logged in";
    public static final String NEW_USER = "New user registered";
    public static final String CHANGE_PASSWORD = " changed password.";
}
