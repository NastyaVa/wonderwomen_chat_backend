package utils.constants;

public class ConstantsMessage {

    public static final String TO = "to";
    public static final String FROM = "from";
    public static final String PAYLOAD = "payload";
    public static final String TIME = "time";
    public static final String COMMAND = "command";
}
