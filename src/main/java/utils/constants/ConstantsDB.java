package utils.constants;

public class ConstantsDB {

    public static final String FIND_BY_LOGIN = "SELECT * from users WHERE login = ?";
    public static final String FIND_BY_EMAIL = "SELECT * from users WHERE email = ?";
    public static final String FIND_ALL_LOGINS = "SELECT login from users";
    public static final String INSERT = "INSERT into users (login, password, email, phone_number, company_name) values (?,?,?,?,?)";
    public static final String UPDATE = "UPDATE users SET login = ?, password = ?, email = ?, phone_number = ?, company_name = ? WHERE login = ? ";
    public static final String DB_COLUMN_LABEL_SERVER = "Server";
    public static final String DB_ADD_TO_PRIVATE_HISTORY = "INSERT INTO %s (id,login,message,time) VALUES (nextval('all_messages_sequence'),?,?,?);";
    public static final String DB_READ_HISTORY = "SELECT * FROM %s";
    public static final String DB_GET_ALL_CHATS_BY_LOGIN = "SELECT \"chat\" FROM users_chats WHERE login = ?";
    public static final String DB_GET_ALL_LOGINS_BY_CHAT_NAME = "SELECT \"login\" FROM users_chats WHERE chat = ?";
    public static final String CHANGE_PASSWORD = "UPDATE users SET  password = ? WHERE login = ? ";
    public final static String INSERT_USERS_IN_TABLE_USERS_AND_CHATS = "INSERT INTO users_chats (id, login, chat)  VALUES ";
    public final static String INSERT_USER_DATA = "(nextval('user_chat_sequence'),?,?)";
    public final static String TABLE_NAME = "%s_%s";
    public final static String DB_SELECT_ALL_CREDENTIALS = "SELECT * FROM pass_and_login";
    public final static String DB_CREATE_TABLE_PRIVAT_CHAT = "CREATE table %s (id serial,login varchar,message varchar,time varchar);";
    public final static String CREATE_TABLE = "Privat chat %s created";
    public final static String GET_ALL_CHAT_NAMES = "SELECT DISTINCT chat FROM users_chats";
    public final static String SEMICOLON = ";";
    public final static String CHAT_NAME_INCORRECT = "Chat name must consist from letters and digits, and contain from 4 to 15 characters.";
    public final static String CHAT_NAME_ALREADY_EXISTS = "Chat name already exists.";

}