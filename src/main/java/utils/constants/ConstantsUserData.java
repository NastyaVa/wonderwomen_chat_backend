package utils.constants;

public class ConstantsUserData {
    public static final String NAME_PROPERTY_FILE = "application.properties";

    //User
    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";
    public static final String EMAIL = "email";
    public static final String PHONE_NUMBER = "phone_number";
    public static final String COMPANY_NAME = "company_name";

}
