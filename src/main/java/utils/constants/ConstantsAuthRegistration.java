package utils.constants;

public class ConstantsAuthRegistration {

    public static final String LOGIN = "login";
    public static final String PASSWORD = "password";

    public static final String INCORRECT_LOGIN = "Login must consist from letters and digits, and contain from 4 to 15 characters.";
    public static final String INCORRECT_PASSWORD = "Password must consist from letters, digits or special characters, and contain from 8 to 15 characters.";
    public static final String INCORRECT_EMAIL = "Email entered incorrectly.";
    public static final String INCORRECT_PHONE = "The phone must contain only numbers. Special characters available '- _():=+'.";
    public static final String INCORRECT_COMPANY = "The company name can contain letters and numbers. Special character available '-'.";

    public static final String REGISTERED = "You are registered";
    public static final String CHANGED_PSW = "Your new password";
    public static final String LOGIN_OR_PASS_NOT_FOUND = "Login or password doesn't match our records.";
    public static final String FIELDS_NULL = "Fields login, password and email cannot be empty.";
    public static final String FIELDS_NULL_L_E = "Fields login and email cannot be empty.";
    public static final String USER_EXISTS = "Such user exists.";
    public static final String USER_LOGGED_IN = " has already logged in.";
    public static final String USER_NOT_FOUND = "User with this login or email was not found.";

}
