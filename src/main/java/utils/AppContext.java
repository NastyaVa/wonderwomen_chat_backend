package utils;

import cache.GetNewID;
import cache.MessagePool;
import cache.SessionCache;
import com.google.gson.Gson;
import database.dao.ChatsHistoryDAO;
import database.dao.PrivateChatsDAO;
import database.dao.impl.UserDAO;
import database.subsidiary.ConnectionProvider;
import database.subsidiary.HashPassword;
import database.subsidiary.RandomPassword;
import models.ChatsUsers;
import server.ServerStarter;
import server.services.servlet.AuthorizationService;
import server.services.servlet.ForgotPasswordService;
import server.services.servlet.RegistrationService;
import server.services.websocket.WebsocketRequestsProcessingService;
import server.services.websocket.command_services.CommandServiceFabric;
import server.services.websocket.subsidiary.ChatUsersGetter;
import utils.validation.ChatNameValidation;
import utils.validation.MessageValidation;
import utils.validation.UserDataValidation;

import java.util.Random;
import java.util.UUID;

public class AppContext {

    public static AppContext appContext = new AppContext();
    private final Property property = new Property();
    private final ConnectionProvider connectionProvider = new ConnectionProvider();
    private final HashPassword hashPassword = new HashPassword(property.getSalt());
    private final Random random = new Random();
    private final RandomPassword randomPassword = new RandomPassword(random);
    private final UserDAO userDAO = new UserDAO(connectionProvider, hashPassword, randomPassword);
    private final ServerStarter serverStarter = new ServerStarter();
    private final UserDataValidation validationUser = new UserDataValidation();
    private final GetNewID getNewID = new GetNewID();
    private final SessionCache sessionCache = new SessionCache(getNewID);
    private final Gson gson = new Gson();
    private final ChatUsersGetter chatUsersGetter = new ChatUsersGetter(sessionCache, userDAO);
    private final ChatsHistoryDAO chatsHistoryDAO = new ChatsHistoryDAO(connectionProvider);
    private final PrivateChatsDAO createPrivateChat = new PrivateChatsDAO(connectionProvider);
    private final ChatNameValidation chatNameValidation = new ChatNameValidation();
    private final ChatsUsers chatsUsers = new ChatsUsers();
    private final CommandServiceFabric commandServiceFabric = new CommandServiceFabric(gson, sessionCache, chatUsersGetter, chatsHistoryDAO, createPrivateChat, chatNameValidation, chatsUsers);
    private final MessagePool messagePool = new MessagePool(commandServiceFabric);
    private final MessageValidation messageValidation = new MessageValidation();
    private final WebsocketRequestsProcessingService websocketRequestsProcessingService = new WebsocketRequestsProcessingService(sessionCache, messagePool, messageValidation);
    private final AuthorizationService authorizationService = new AuthorizationService(gson, userDAO, validationUser, sessionCache, hashPassword);
    private final RegistrationService registrationService = new RegistrationService(gson, userDAO, validationUser, sessionCache);
    private final ForgotPasswordService forgotPasswordService = new ForgotPasswordService(gson, userDAO);

    public HashPassword getHashPassword() {
        return hashPassword;
    }

    public UserDataValidation getValidationUser() {
        return validationUser;
    }

    public UserDAO getUserDAO() {
        return userDAO;
    }

    public AuthorizationService getAuthorizationService() {
        return authorizationService;
    }

    public ForgotPasswordService getForgotPasswordService() {
        return forgotPasswordService;
    }

    public RegistrationService getRegistrationService() {
        return registrationService;
    }

    public ServerStarter getServerStarter() {
        return serverStarter;
    }

    public Property getProperty() {
        return property;
    }

    public SessionCache getSessionCache() {
        return sessionCache;
    }

    public Gson getGson() {
        return gson;
    }

    public WebsocketRequestsProcessingService getWebsocketRequestsProcessingService() {
        return websocketRequestsProcessingService;
    }


    public static AppContext getAppContext() {
        return appContext;
    }

}