package database.subsidiary;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;
import java.util.Random;

public class RandomPasswordTest {

    Random random = Mockito.mock(Random.class);
    RandomPassword randomPassword = new RandomPassword(random);

    @Test
    public void getRandomPassword() {
        String login = "user";
        Mockito.when(random.nextInt(999999999)).thenReturn(999999999);

        String expectedNewPassword = "u999999999r";
        String actualNewPassword = randomPassword.getRandomPassword(login);

        Assert.assertEquals(expectedNewPassword, actualNewPassword);
    }
}