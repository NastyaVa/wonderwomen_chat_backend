package database.dao;

import database.subsidiary.ConnectionProvider;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static utils.constants.ConstantsDB.*;

public class PrivateChatsDAOTest {

    String tableName = "tableName";
    private final Connection connection = Mockito.mock(Connection.class);
    ConnectionProvider connectionProvider = Mockito.mock(ConnectionProvider.class);
    PreparedStatement statement = Mockito.mock(PreparedStatement.class);
    ResultSet resultSet = Mockito.mock(ResultSet.class);

    PrivateChatsDAO privateChatsDAO = new PrivateChatsDAO(connectionProvider);

    @Test
    public void checkIfChatNameExists() throws SQLException {
        Mockito.when(connectionProvider.getConnection()).thenReturn(connection);
        Mockito.when(connection.prepareStatement(GET_ALL_CHAT_NAMES)).thenReturn(statement);
        Mockito.when(statement.execute()).thenReturn(true);
        Mockito.when(statement.getResultSet()).thenReturn(resultSet);
        Mockito.when(resultSet.next()).thenReturn(true);
        Mockito.when(resultSet.getString("chat")).thenReturn(tableName);

        Assert.assertTrue(privateChatsDAO.checkIfChatNameExists(tableName));
    }

    @Test
    public void addLoginsAndChatName() {

    }
}