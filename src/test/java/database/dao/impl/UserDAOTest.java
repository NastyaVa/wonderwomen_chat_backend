package database.dao.impl;

import database.subsidiary.ConnectionProvider;
import database.subsidiary.HashPassword;
import database.subsidiary.RandomPassword;
import models.User;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import utils.constants.ConstantsUserData;
import utils.constants.ConstantsDB;
import java.sql.*;
import java.util.LinkedList;
import java.util.List;

class UserDAOTest {

    private final ConnectionProvider connectionProvider = Mockito.mock(ConnectionProvider.class);
    private final HashPassword hashPassword = Mockito.mock(HashPassword.class);
    private final RandomPassword randomPassword = Mockito.mock(RandomPassword.class);
    private final UserDAO userDAO = new UserDAO (connectionProvider, hashPassword, randomPassword);
    private final Connection connection = Mockito.mock(Connection.class);
    private final PreparedStatement preparedStatement = Mockito.mock(PreparedStatement.class);
    private final ResultSet resultSet = Mockito.mock(ResultSet.class);
    private final Statement statement = Mockito.mock(Statement.class);
    private final String login = "user1";
    private final String password = "fhght1";
    private final String hashPSW = "d23015cac44602ae85fe6afea9f61d7b6a25a68987a9702e915840076a434d58";
    private final String email = "test1@mail.tu";
    private final String phoneNumber = "56879078";
    private final String companyName = "company1";
    private final User user = new User(login, password, email, phoneNumber, companyName);

    @Test
    void findByLoginTest_POSITIVE() throws SQLException {
        User expectedUser = new User(login, password, email, phoneNumber, companyName);

        Mockito.when(connectionProvider.getConnection()).thenReturn(connection);
        Mockito.when(connectionProvider.getPreparedStatement(ConstantsDB.FIND_BY_LOGIN)).thenReturn(preparedStatement);
        Mockito.when(preparedStatement.executeQuery()).thenReturn(resultSet);
        Mockito.when(resultSet.next()).thenReturn(true);
        Mockito.when(resultSet.getString(ConstantsUserData.LOGIN)).thenReturn(login);
        Mockito.when(resultSet.getString(ConstantsUserData.PASSWORD)).thenReturn(password);
        Mockito.when(resultSet.getString(ConstantsUserData.EMAIL)).thenReturn(email);
        Mockito.when(resultSet.getString(ConstantsUserData.PHONE_NUMBER)).thenReturn(phoneNumber);
        Mockito.when(resultSet.getString(ConstantsUserData.COMPANY_NAME)).thenReturn(companyName);

        User user = userDAO.findByLogin(login);

        Assertions.assertEquals(expectedUser, user);
    }

    @Test
    void findByLoginTest_NEGATIVE() throws SQLException {
        Mockito.when(connectionProvider.getConnection()).thenReturn(connection);
        Mockito.when(connectionProvider.getPreparedStatement(ConstantsDB.FIND_BY_LOGIN)).thenReturn(preparedStatement);
        Mockito.when(preparedStatement.executeQuery()).thenReturn(resultSet);
        Mockito.when(resultSet.next()).thenReturn(false);

        User user = userDAO.findByLogin("login");

        Assertions.assertNull(user);
    }

    @Test
    void findByEmailTest_POSITIVE() throws SQLException {
        User expectedUser = new User(login, password, email, phoneNumber, companyName);

        Mockito.when(connectionProvider.getConnection()).thenReturn(connection);
        Mockito.when(connectionProvider.getPreparedStatement(ConstantsDB.FIND_BY_LOGIN)).thenReturn(preparedStatement);
        Mockito.when(preparedStatement.executeQuery()).thenReturn(resultSet);
        Mockito.when(resultSet.next()).thenReturn(true);
        Mockito.when(resultSet.getString(ConstantsUserData.LOGIN)).thenReturn(login);
        Mockito.when(resultSet.getString(ConstantsUserData.PASSWORD)).thenReturn(password);
        Mockito.when(resultSet.getString(ConstantsUserData.EMAIL)).thenReturn(email);
        Mockito.when(resultSet.getString(ConstantsUserData.PHONE_NUMBER)).thenReturn(phoneNumber);
        Mockito.when(resultSet.getString(ConstantsUserData.COMPANY_NAME)).thenReturn(companyName);

        User user = userDAO.findByLogin(email);

        Assertions.assertEquals(expectedUser, user);
    }

    @Test
    void findByEmailTest_NEGATIVE() throws SQLException {
        Mockito.when(connectionProvider.getConnection()).thenReturn(connection);
        Mockito.when(connectionProvider.getPreparedStatement(ConstantsDB.FIND_BY_LOGIN)).thenReturn(preparedStatement);
        Mockito.when(preparedStatement.executeQuery()).thenReturn(resultSet);
        Mockito.when(resultSet.next()).thenReturn(false);

        User user = userDAO.findByLogin("email@mail.com");

        Assertions.assertNull(user);
    }

    @Test
    void changePasswordTest_POSITIVE() throws SQLException {
        String newPassword = "u1518444961";
        Mockito.when(randomPassword.getRandomPassword(login)).thenReturn(newPassword);
        Mockito.when(connectionProvider.getConnection()).thenReturn(connection);
        Mockito.when(connectionProvider.getPreparedStatement(ConstantsDB.CHANGE_PASSWORD)).thenReturn(preparedStatement);
        Mockito.when(preparedStatement.executeQuery()).thenReturn(resultSet);
        Mockito.when(resultSet.next()).thenReturn(true);
        Mockito.when(hashPassword.getHash(newPassword)).thenReturn(hashPSW);
        Mockito.when(resultSet.getString(ConstantsUserData.PASSWORD)).thenReturn(password);
        Mockito.when(resultSet.getString(ConstantsUserData.LOGIN)).thenReturn(login);

        String expectedChangePassword = userDAO.changePassword(login);

        Mockito.verify(preparedStatement, Mockito.times(1)).setString(1, hashPassword.getHash(newPassword));
        Mockito.verify(preparedStatement, Mockito.times(1)).setString(2, login);

        Assertions.assertEquals(expectedChangePassword, newPassword);
    }


    @Test
    void createUserTest_POSITIVE() throws SQLException {
        Mockito.when(connectionProvider.getConnection()).thenReturn(connection);
        Mockito.when(connectionProvider.getPreparedStatement(ConstantsDB.INSERT)).thenReturn(preparedStatement);
        Mockito.when(connectionProvider.getPreparedStatement(ConstantsDB.FIND_BY_LOGIN)).thenReturn(preparedStatement);
        Mockito.when(preparedStatement.executeUpdate()).thenReturn(1);
        Mockito.when(preparedStatement.executeQuery()).thenReturn(resultSet);
        Mockito.when(resultSet.next()).thenReturn(false);
        Mockito.when(hashPassword.getHash(user.getPassword())).thenReturn(hashPSW);

        Boolean result = userDAO.createUser(user);

        Mockito.verify(preparedStatement, Mockito.times(2)).setString(1, user.getLogin());
        Mockito.verify(preparedStatement, Mockito.times(1)).setString(2, hashPassword.getHash(user.getPassword()));
        Mockito.verify(preparedStatement, Mockito.times(1)).setString(3, user.getEmail());
        Mockito.verify(preparedStatement, Mockito.times(1)).setString(4, user.getPhoneNumber());
        Mockito.verify(preparedStatement, Mockito.times(1)).setString(5, user.getCompanyName());

        Assertions.assertTrue(result);
    }

    @Test
    void createUserTest_NEGATIVE() throws SQLException {
        Mockito.when(connectionProvider.getConnection()).thenReturn(connection);
        Mockito.when(connectionProvider.getPreparedStatement(ConstantsDB.FIND_BY_LOGIN)).thenReturn(preparedStatement);
        Mockito.when(preparedStatement.executeQuery()).thenReturn(resultSet);
        Mockito.when(resultSet.next()).thenReturn(true);
        Mockito.when(resultSet.getString(ConstantsUserData.LOGIN)).thenReturn(login);
        Mockito.when(resultSet.getString(ConstantsUserData.PASSWORD)).thenReturn(password);
        Mockito.when(resultSet.getString(ConstantsUserData.EMAIL)).thenReturn(email);
        Mockito.when(resultSet.getString(ConstantsUserData.PHONE_NUMBER)).thenReturn(phoneNumber);
        Mockito.when(resultSet.getString(ConstantsUserData.COMPANY_NAME)).thenReturn(companyName);

        Boolean result = userDAO.createUser(user);

        Assertions.assertFalse(result);
    }

    @Test
    void updateUserTest_POSITIVE() throws SQLException {
        String newLogin = "qwerty";
        String newPassword = "9r4n8";
        String newEmail = "ficn@go.com";
        String newPhoneNumber = "76845467";
        String newCompanyName = "GERTV";

        Mockito.when(connectionProvider.getConnection()).thenReturn(connection);
        Mockito.when(connectionProvider.getPreparedStatement(ConstantsDB.FIND_BY_LOGIN)).thenReturn(preparedStatement);
        Mockito.when(connectionProvider.getPreparedStatement(ConstantsDB.UPDATE)).thenReturn(preparedStatement);
        Mockito.when(preparedStatement.executeQuery()).thenReturn(resultSet);
        Mockito.when(resultSet.next()).thenReturn(true);
        Mockito.when(resultSet.getString(ConstantsUserData.LOGIN)).thenReturn(login);
        Mockito.when(resultSet.getString(ConstantsUserData.PASSWORD)).thenReturn(password);
        Mockito.when(resultSet.getString(ConstantsUserData.EMAIL)).thenReturn(email);
        Mockito.when(resultSet.getString(ConstantsUserData.PHONE_NUMBER)).thenReturn(phoneNumber);
        Mockito.when(resultSet.getString(ConstantsUserData.COMPANY_NAME)).thenReturn(companyName);
        Mockito.when(hashPassword.getHash(newPassword)).thenReturn(hashPSW);

        Boolean result = userDAO.updateUser(user, newLogin, newPassword, newEmail, newPhoneNumber, newCompanyName );


        Mockito.verify(preparedStatement, Mockito.times(1)).setString(1, newLogin);
        Mockito.verify(preparedStatement, Mockito.times(1)).setString(2, hashPassword.getHash(newPassword));
        Mockito.verify(preparedStatement, Mockito.times(1)).setString(3, newEmail);
        Mockito.verify(preparedStatement, Mockito.times(1)).setString(4, newPhoneNumber);
        Mockito.verify(preparedStatement, Mockito.times(1)).setString(5, newCompanyName);

        Assertions.assertTrue(result);
    }

    @Test
    void updateUserTest_NEGATIVE() throws SQLException {
        Mockito.when(connectionProvider.getConnection()).thenReturn(connection);
        Mockito.when(connectionProvider.getPreparedStatement(ConstantsDB.FIND_BY_LOGIN)).thenReturn(preparedStatement);
        Mockito.when(connectionProvider.getPreparedStatement(ConstantsDB.UPDATE)).thenReturn(preparedStatement);
        Mockito.when(preparedStatement.executeQuery()).thenReturn(resultSet);
        Mockito.when(resultSet.next()).thenReturn(false);

        String newLogin = "qwerty";
        String newPassword = "9r4n8";
        String newEmail = "ficn@go.com";
        String newPhoneNumber = "76845467";
        String newCompanyName = "GERTV";
        Boolean result = userDAO.updateUser(user, newLogin, newPassword, newEmail, newPhoneNumber, newCompanyName);

        Assertions.assertFalse(result);
    }

    @Test
    void authorizationTest_POSITIVE() throws SQLException {
        Mockito.when(connectionProvider.getConnection()).thenReturn(connection);
        Mockito.when(connectionProvider.getPreparedStatement(ConstantsDB.FIND_BY_LOGIN)).thenReturn(preparedStatement);
        Mockito.when(preparedStatement.executeQuery()).thenReturn(resultSet);
        Mockito.when(resultSet.next()).thenReturn(true);
        Mockito.when(resultSet.getString(ConstantsUserData.LOGIN)).thenReturn(login);
        Mockito.when(resultSet.getString(ConstantsUserData.PASSWORD)).thenReturn(password);
        Mockito.when(resultSet.getString(ConstantsUserData.EMAIL)).thenReturn(email);
        Mockito.when(resultSet.getString(ConstantsUserData.PHONE_NUMBER)).thenReturn(phoneNumber);
        Mockito.when(resultSet.getString(ConstantsUserData.COMPANY_NAME)).thenReturn(companyName);

        String result = userDAO.authorization(login);
        String expectedPassword = "fhght1";

        Assertions.assertEquals(expectedPassword, result);
    }

    @Test
    void authorizationTest_NEGATIVE() throws SQLException {

        Mockito.when(connectionProvider.getConnection()).thenReturn(connection);
        Mockito.when(connectionProvider.getPreparedStatement(ConstantsDB.FIND_BY_LOGIN)).thenReturn(preparedStatement);
        Mockito.when(preparedStatement.executeQuery()).thenReturn(resultSet);
        Mockito.when(resultSet.next()).thenReturn(false);

        String result = userDAO.authorization(login);

        Assertions.assertNull(result);

    }

    @Test
    void getAllUserLoginsTest_POSITIVE() throws SQLException {
        String login1 = "login 1";
        String login2 = "login 2";
        List<String> loginList = new LinkedList<>();
        loginList.add(login1);
        loginList.add(login2);

        Mockito.when(connectionProvider.getConnection()).thenReturn(connection);
        Mockito.when(connection.createStatement()).thenReturn(statement);
        Mockito.when(statement.executeQuery(ConstantsDB.FIND_ALL_LOGINS)).thenReturn(resultSet);
        Mockito.when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        Mockito.when(resultSet.getString(1)).thenReturn(login1).thenReturn(login2);

        List<String> actualLoginList = userDAO.getAllUserLogins();

        Assertions.assertEquals(loginList , actualLoginList);
    }
}