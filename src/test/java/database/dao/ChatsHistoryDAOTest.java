package database.dao;

import database.subsidiary.ConnectionProvider;
import models.Message;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

import static utils.constants.ConstantsDB.*;

public class ChatsHistoryDAOTest {
    Message message = Mockito.mock(Message.class);
    PreparedStatement statement = Mockito.mock(PreparedStatement.class);
    ConnectionProvider connectionProvider = Mockito.mock(ConnectionProvider.class);
    ResultSet resultSet = Mockito.mock(ResultSet.class);
    Connection connection = Mockito.mock(Connection.class);

    ChatsHistoryDAO chatsHistoryDAO = new ChatsHistoryDAO(connectionProvider);

    @Test
    public void saveMessages() throws SQLException {
        Mockito.when(connectionProvider.getConnection()).thenReturn(connection);
        Mockito.when(connectionProvider.getConnection()
                .prepareStatement(String.format(DB_ADD_TO_PRIVATE_HISTORY,
                        message.getTo()))).thenReturn(statement);
        Mockito.when(statement.executeQuery()).thenReturn(resultSet);
        Mockito.when(resultSet.next()).thenReturn(true);

        Mockito.when(message.getFrom()).thenReturn("From");
        Mockito.when(message.getPayload()).thenReturn("Payload");
        Mockito.when(message.getTime()).thenReturn(123456L);

        Assertions.assertTrue(chatsHistoryDAO.saveMessages(message));
    }

    @Test
    public void getListMessages() throws SQLException {
        Queue<Message> listMessages = new ArrayDeque<>();
        String from = "FROM";
        String chatName = "chatName";
        String body = "BODY";
        long time = 123456L;

        Message message1 = new Message(from, chatName, body, time);
        listMessages.add(message1);

        int fromColumnNumber = 2;
        int payloadColumnNumber = 3;
        int timeColumnNumber = 4;

        Mockito.when(connectionProvider.getConnection()).thenReturn(connection);
        Mockito.when(connectionProvider.getConnection().createStatement()).thenReturn(statement);
        Mockito.when(statement.executeQuery(String.format(DB_READ_HISTORY, chatName))).thenReturn(resultSet);
        Mockito.when(resultSet.next()).thenReturn(true).thenReturn(false);
        Mockito.when(resultSet.getString(fromColumnNumber)).thenReturn(from);
        Mockito.when(resultSet.getString(payloadColumnNumber)).thenReturn(body);
        Mockito.when( resultSet.getLong(timeColumnNumber)).thenReturn(time);

        Queue<Message> actualListMessages = chatsHistoryDAO.getListMessages(chatName);

        Assertions.assertEquals(actualListMessages.peek().getFrom(), listMessages.peek().getFrom());
        Assertions.assertEquals(actualListMessages.peek().getTo(), listMessages.peek().getTo());
        Assertions.assertEquals(actualListMessages.peek().getPayload(), listMessages.peek().getPayload());
        Assertions.assertEquals((actualListMessages.peek().getTime()), listMessages.peek().getTime());

    }

    @Test
    public void getChatsByLogin() throws SQLException {
        String login = "login1";
        String chat1 = "chat1";
        String chat2 = "chat2";
        int chatColumnNumber = 1;
        List<String> chats = new ArrayList<>();
        chats.add(chat1);
        chats.add(chat2);

        Mockito.when(connectionProvider.getConnection()).thenReturn(connection);
        Mockito.when(connection.prepareStatement(DB_GET_ALL_CHATS_BY_LOGIN)).thenReturn(statement);
        Mockito.when(statement.executeQuery()).thenReturn(resultSet);
        Mockito.when(resultSet.next()).thenReturn(true).thenReturn(true).thenReturn(false);
        Mockito.when(resultSet.getString(1)).thenReturn(login);
        Mockito.when(resultSet.getString(chatColumnNumber)).thenReturn(chat1).thenReturn(chat2);

        Assertions.assertEquals(chats, chatsHistoryDAO.getChatsByLogin(login));
    }
}