package server.services.websocket;

import cache.MessagePool;
import cache.SessionCache;
import database.dao.ChatsHistoryDAO;
import models.LoginAndSession;
import models.Message;
import models.MessageData;
import org.eclipse.jetty.websocket.api.Session;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;
import org.mockito.Mockito;
import server.services.websocket.WebsocketRequestsProcessingService;
import utils.validation.MessageValidation;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.times;
import static utils.enums.Commands.GENERAL_CHAT;

public class WebsocketRequestsProcessingServiceTest {

    private static final Session SESSION_1 = Mockito.mock(Session.class);
    private static final String MESSAGE_STRING_1 = "{\"payload\":\"Hi\",\"to\":\"general_chat\",\"time\":1617390212306,\"command\":\"GENERAL_CHAT\"}";
    private static final Message MESSAGE_1 = new Message("Alla", "general_chat", "Hi", 1617390212306L);
    private static final UUID UUID_1 = UUID.fromString("879936a4-bb86-4451-ba62-0f44def6f14e");
    private static final MessageData MESSAGE_DATA_1 = new MessageData(MESSAGE_1, UUID_1, GENERAL_CHAT);
    private static final Session SESSION_2 = Mockito.mock(Session.class);
    private static final String MESSAGE_STRING_2 = "{\"payload\":\"Hello\",\"to\":\"general_chat\",\"time\":77777777,\"command\":\"GENERAL_CHAT\"}";
    private static final Message MESSAGE_2 = new Message("Kolya", "general_chat", "Hello", 77777777L);
    private static final UUID UUID_2 = UUID.fromString("8c9587d8-69b4-465d-9547-3d242f91887b");
    private static final MessageData MESSAGE_DATA_2 = new MessageData(MESSAGE_2, UUID_2, GENERAL_CHAT);
    private SessionCache sessionCache = Mockito.mock(SessionCache.class);
    private ChatsHistoryDAO chatsHistoryDAO = Mockito.mock(ChatsHistoryDAO.class);
    private MessagePool messagePool = Mockito.mock(MessagePool.class);
    private MessageValidation messageValidation = Mockito.mock(MessageValidation.class);
    private WebsocketRequestsProcessingService cut = new WebsocketRequestsProcessingService(sessionCache, messagePool, messageValidation);

    private JSONObject jsonObject = new JSONObject(MESSAGE_STRING_1);
    private ConcurrentHashMap<UUID, LoginAndSession> cache = new ConcurrentHashMap<>();
    private LoginAndSession loginAndSession_1 = new LoginAndSession("Alla");
    private LoginAndSession loginAndSession_2 = new LoginAndSession("Kolya");

    private static Stream<Arguments> streamForValid() {
        return Stream.of(
                Arguments.arguments(SESSION_1, UUID_1, MESSAGE_STRING_1, MESSAGE_DATA_1),
                Arguments.arguments(SESSION_2, UUID_2, MESSAGE_STRING_2, MESSAGE_DATA_2)
        );
    }

    @BeforeEach
    void setUp() {
        loginAndSession_1.setSession(SESSION_1);
        cache.put(UUID_1, loginAndSession_1);
        loginAndSession_2.setSession(SESSION_2);
        cache.put(UUID_2, loginAndSession_2);
    }

    @Test
    void onTextServiceTest_SESSION_NOT_FOUND() throws IOException {
        cut.onTextService(SESSION_1, MESSAGE_STRING_1);
        Mockito.verify(messagePool, never()).addMessage(any(MessageData.class));
    }

    @ParameterizedTest
    @MethodSource("streamForValid")
    void onTextServiceTest_VALID_MESSAGE(Session session, UUID uuid, String messageString, MessageData messageData) throws IOException {
        Mockito.when(sessionCache.getUUIDBySession(session)).thenReturn(uuid);
        Mockito.when(messageValidation.validateMessageContent(any())).thenReturn(true);
        Mockito.when(messageValidation.validateMessageLength(any())).thenReturn(true);
        Mockito.when(sessionCache.getCache()).thenReturn(cache);

        cut.onTextService(session, messageString);

        Mockito.verify(messagePool, times(1)).addMessage(messageData);
    }

    @Test
    void onTextServiceTest_MESSAGE_CONTENT_INCORRECT() {
        Mockito.when(sessionCache.getUUIDBySession(SESSION_1)).thenReturn(UUID_1);
        //   cut.onTextService(session, messageIncorrectContentString);
        //    Mockito.when(messageValidation.validateMessageContent(jsonObject)).thenReturn(false);
        //    Mockito.verify(messagePool, times(0)).addMessage(Mockito.any(MessageData.class));
        Assertions.assertFalse(messageValidation.validateMessageContent(jsonObject));
    }

}