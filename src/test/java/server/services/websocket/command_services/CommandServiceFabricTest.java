package server.services.websocket.command_services;

import cache.SessionCache;
import com.google.gson.Gson;
import database.dao.ChatsHistoryDAO;
import database.dao.PrivateChatsDAO;
import models.ChatsUsers;
import org.checkerframework.checker.units.qual.C;
import org.junit.Test;
import org.mockito.Mockito;
import server.services.websocket.command_services.impl.DefaultService;
import server.services.websocket.command_services.impl.GeneralChatService;
import server.services.websocket.subsidiary.ChatUsersGetter;
import utils.enums.Commands;
import utils.validation.ChatNameValidation;

import static org.junit.jupiter.api.Assertions.*;

public class CommandServiceFabricTest {

    Gson gson = new Gson();
    SessionCache sessionCache = Mockito.mock(SessionCache.class);
    ChatUsersGetter chatUsersGetter = Mockito.mock(ChatUsersGetter.class);
    ChatsHistoryDAO chatsHistoryDAO = Mockito.mock(ChatsHistoryDAO.class);
    PrivateChatsDAO privateChatsDAO = Mockito.mock(PrivateChatsDAO.class);
    ChatNameValidation chatNameValidation = Mockito.mock(ChatNameValidation.class);
    ChatsUsers chatsUsers = Mockito.mock(ChatsUsers.class);


    CommandServiceFabric commandServiceFabric = new CommandServiceFabric(gson, sessionCache, chatUsersGetter, chatsHistoryDAO, privateChatsDAO, chatNameValidation, chatsUsers);
    ICommandService generalChatService = new GeneralChatService(gson, sessionCache, chatsHistoryDAO);
    ICommandService defaultService = new DefaultService();

    @Test
    public void chooseSessionTest_GENERAL_CHAT_SESSION() {
        ICommandService actual = commandServiceFabric.chooseSession(Commands.GENERAL_CHAT);
        assertEquals(generalChatService, actual);
    }

    @Test
    public void chooseSessionTest_NOT_FOUND_COMMAND() {
        ICommandService actual = commandServiceFabric.chooseSession(Commands.DEFAULT_COMMAND);
        assertEquals(defaultService, actual);
    }

}