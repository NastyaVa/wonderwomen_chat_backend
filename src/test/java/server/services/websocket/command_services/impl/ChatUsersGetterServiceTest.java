package server.services.websocket.command_services.impl;

import cache.SessionCache;
import com.google.gson.Gson;
import models.LoginAndSession;
import models.Message;
import models.UserLoginAndIsOnline;
import models.UserPC;
import org.eclipse.jetty.websocket.api.RemoteEndpoint;
import org.eclipse.jetty.websocket.api.Session;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;
import server.services.websocket.subsidiary.ChatUsersGetter;
import utils.enums.Commands;
import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import static org.mockito.Mockito.times;

public class ChatUsersGetterServiceTest {

    Gson gson = new Gson();
    ChatUsersGetter chatUsersGetter = Mockito.mock(ChatUsersGetter.class);
    SessionCache sessionCache = Mockito.mock(SessionCache.class);
    UUID uuid = UUID.fromString("8af3633e-dfac-4956-a2b2-f6fa25476206");
    Session session = Mockito.mock(Session.class);
    ChatUsersGetterService chatUsersGetterService = new ChatUsersGetterService(gson, chatUsersGetter, sessionCache);
    Message message = new Message();
    RemoteEndpoint remoteEndpoint = Mockito.mock(RemoteEndpoint.class);
    UserPC userPC = new UserPC(Commands.CHAT_USERS_GETTER.toString(), message);

    @Test
    public void sendMessageTest_MULTIPLE_USERS_ONLINE() throws IOException {
        UserLoginAndIsOnline userLoginAndIsOnline1 = new UserLoginAndIsOnline("userLogin");
        userLoginAndIsOnline1.setOnline(true);
        UserLoginAndIsOnline userLoginAndIsOnline2 = new UserLoginAndIsOnline("login2");
        UserLoginAndIsOnline userLoginAndIsOnline3 = new UserLoginAndIsOnline("login3");
        UserLoginAndIsOnline userLoginAndIsOnline4 = new UserLoginAndIsOnline("login4");
        userLoginAndIsOnline4.setOnline(true);
        List<UserLoginAndIsOnline> list = Arrays.asList(userLoginAndIsOnline1, userLoginAndIsOnline2, userLoginAndIsOnline3, userLoginAndIsOnline4);
        LoginAndSession loginAndSession = new LoginAndSession("userLogin");
        loginAndSession.setSession(session);
        Mockito.when(sessionCache.findByUUID(uuid)).thenReturn(loginAndSession);
        Mockito.when(chatUsersGetter.getUsersList()).thenReturn(list);
        Mockito.when(session.getRemote()).thenReturn(remoteEndpoint);
        chatUsersGetterService.sendMessage(message, uuid);
        Mockito.verify(remoteEndpoint, times(1)).sendString(gson.toJson(userPC));
        Assertions.assertTrue(chatUsersGetterService.sendMessage(message, uuid));
    }

    @Test
    public void sendMessageTest_ONE_USER_ONLINE() throws IOException {
        UserLoginAndIsOnline userLoginAndIsOnline1 = new UserLoginAndIsOnline("userLogin");
        userLoginAndIsOnline1.setOnline(true);
        List<UserLoginAndIsOnline> list = Collections.singletonList(userLoginAndIsOnline1);
        LoginAndSession loginAndSession = new LoginAndSession("userLogin");
        loginAndSession.setSession(session);
        Mockito.when(sessionCache.findByUUID(uuid)).thenReturn(loginAndSession);
        Mockito.when(chatUsersGetter.getUsersList()).thenReturn(list);
        Mockito.when(session.getRemote()).thenReturn(remoteEndpoint);
        chatUsersGetterService.sendMessage(message, uuid);
        Mockito.verify(remoteEndpoint, times(1)).sendString(gson.toJson(userPC));
        Assertions.assertTrue(chatUsersGetterService.sendMessage(message, uuid));
    }

    @Test
    public void sendMessageTest_NO_USERS_ONLINE() {
        Assertions.assertFalse(chatUsersGetterService.sendMessage(message, uuid));

    }
}