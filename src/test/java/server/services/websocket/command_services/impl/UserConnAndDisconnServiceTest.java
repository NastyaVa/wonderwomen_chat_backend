package server.services.websocket.command_services.impl;

import cache.SessionCache;
import com.google.gson.Gson;
import models.LoginAndSession;
import models.Message;
import models.UserPC;
import org.eclipse.jetty.websocket.api.RemoteEndpoint;
import org.eclipse.jetty.websocket.api.Session;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;

import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import static org.mockito.Mockito.times;
import static utils.constants.ConstantsWebSocket.USER_JOINED_THE_CHAT;
import static utils.constants.ConstantsWebSocket.USER_LEFT_THE_CHAT;
import static utils.enums.Commands.NEW_USER_CONNECTED;
import static utils.enums.Commands.USER_DISCONNECTED;

public class UserConnAndDisconnServiceTest {

    UUID uuid1 = UUID.fromString("8af3633e-dfac-4956-a2b2-f6fa25476206");
    Gson gson = new Gson();
    Message message = new Message();
    SessionCache sessionCache = Mockito.mock(SessionCache.class);
    private ConcurrentHashMap<UUID, LoginAndSession> cache = new ConcurrentHashMap<>();
    Session session = Mockito.mock(Session.class);
    RemoteEndpoint remoteEndpoint = Mockito.mock(RemoteEndpoint.class);
    UserPC userPCNewUser = new UserPC(NEW_USER_CONNECTED.toString(), message);
    UserPC userPCUserLeft = new UserPC(USER_DISCONNECTED.toString(), message);

    UserConnAndDisconnService cut = new UserConnAndDisconnService(gson, sessionCache);

    @Test
    public void sendMessageTest_USER_JOINED_THE_CHAT_WHEN_ONE_USER_ONLINE() {

        message.setPayload(USER_JOINED_THE_CHAT);

        UUID uuid1 = UUID.fromString("8af3633e-dfac-4956-a2b2-f6fa25476206");

        LoginAndSession loginAndSession1 = new LoginAndSession("login1");
        loginAndSession1.setSession(session);

        cache.put(uuid1, loginAndSession1);

        Mockito.when(sessionCache.getCache()).thenReturn(cache);
        Mockito.when(session.getRemote()).thenReturn(remoteEndpoint);

        Assertions.assertFalse(cut.sendMessage(message, uuid1));
    }

    @Test
    public void sendMessageTest_USER_JOINED_THE_CHAT_WHEN_MULTIPLE_USERS_ONLINE() throws IOException {
        UUID uuid2 = UUID.fromString("3d4842e5-2be5-4fce-9ff5-660f63d199c3");
        UUID uuid3 = UUID.fromString("5d5dd2ce-54d5-411b-a2dc-40ff8334b941");
        LoginAndSession loginAndSession1 = new LoginAndSession("login1");
        loginAndSession1.setSession(session);
        LoginAndSession loginAndSession2 = new LoginAndSession("login2");
        Session session2 = Mockito.mock(Session.class);
        loginAndSession2.setSession(session2);
        LoginAndSession loginAndSession3 = new LoginAndSession("login3");
        Session session3 = Mockito.mock(Session.class);
        loginAndSession3.setSession(session3);

        cache.put(uuid1, loginAndSession1);
        cache.put(uuid2, loginAndSession2);
        cache.put(uuid3, loginAndSession3);

        message.setPayload(USER_JOINED_THE_CHAT);

        Mockito.when(sessionCache.getCache()).thenReturn(cache);
        Mockito.when(session.getRemote()).thenReturn(remoteEndpoint);
        Mockito.when(session2.getRemote()).thenReturn(remoteEndpoint);
        Mockito.when(session3.getRemote()).thenReturn(remoteEndpoint);
        cut.sendMessage(message, uuid1);
        Mockito.verify(remoteEndpoint, times(2)).sendString(gson.toJson(userPCNewUser));
        Assertions.assertTrue(cut.sendMessage(message, uuid1));
    }

    @Test
    public void sendMessageTest_USER_LEFT_THE_CHAT_WHEN_ONE_USER_ONLINE() {

        message.setPayload(USER_LEFT_THE_CHAT);

        UUID uuid1 = UUID.fromString("8af3633e-dfac-4956-a2b2-f6fa25476206");

        LoginAndSession loginAndSession1 = new LoginAndSession("login1");
        loginAndSession1.setSession(session);

        cache.put(uuid1, loginAndSession1);

        Mockito.when(sessionCache.getCache()).thenReturn(cache);
        Mockito.when(session.getRemote()).thenReturn(remoteEndpoint);

        Assertions.assertFalse(cut.sendMessage(message, uuid1));
    }

    @Test
    public void sendMessageTest_USER_LEFT_THE_CHAT_WHEN_MULTIPLE_USERS_ONLINE() throws IOException {
        UUID uuid2 = UUID.fromString("3d4842e5-2be5-4fce-9ff5-660f63d199c3");
        UUID uuid3 = UUID.fromString("5d5dd2ce-54d5-411b-a2dc-40ff8334b941");
        LoginAndSession loginAndSession1 = new LoginAndSession("login1");
        loginAndSession1.setSession(session);
        LoginAndSession loginAndSession2 = new LoginAndSession("login2");
        Session session2 = Mockito.mock(Session.class);
        loginAndSession2.setSession(session2);
        LoginAndSession loginAndSession3 = new LoginAndSession("login3");
        Session session3 = Mockito.mock(Session.class);
        loginAndSession3.setSession(session3);

        cache.put(uuid1, loginAndSession1);
        cache.put(uuid2, loginAndSession2);
        cache.put(uuid3, loginAndSession3);

        message.setPayload(USER_LEFT_THE_CHAT);

        Mockito.when(sessionCache.getCache()).thenReturn(cache);
        Mockito.when(session.getRemote()).thenReturn(remoteEndpoint);
        Mockito.when(session2.getRemote()).thenReturn(remoteEndpoint);
        Mockito.when(session3.getRemote()).thenReturn(remoteEndpoint);
        cut.sendMessage(message, uuid1);
        Mockito.verify(remoteEndpoint, times(2)).sendString(gson.toJson(userPCUserLeft));
        Assertions.assertTrue(cut.sendMessage(message, uuid1));
    }

    @Test
    public void sendMessage_NO_USERS_ONLINE() {
        Mockito.when(sessionCache.getCache()).thenReturn(cache);
        cut.sendMessage(message, uuid1);
        Assertions.assertFalse(cut.sendMessage(message, uuid1));
    }
}