package server.services.websocket.command_services.impl;

import cache.SessionCache;
import com.google.gson.Gson;
import database.dao.ChatsHistoryDAO;
import models.LoginAndSession;
import models.Message;
import models.UserPC;
import org.eclipse.jetty.websocket.api.RemoteEndpoint;
import org.eclipse.jetty.websocket.api.Session;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;
import utils.enums.Commands;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;
import static org.mockito.Mockito.times;
import static utils.enums.Commands.INCORRECT_MESSAGE;

public class GeneralChatServiceTest {

    UUID uuid = UUID.fromString("8af3633e-dfac-4956-a2b2-f6fa25476206");
    Gson gson = new Gson();
    Message message = new Message();
    SessionCache sessionCache = Mockito.mock(SessionCache.class);
    LoginAndSession loginAndSession = Mockito.mock(LoginAndSession.class);
    Session session = Mockito.mock(Session.class);
    ChatsHistoryDAO chatsHistoryDAO = Mockito.mock(ChatsHistoryDAO.class);
    RemoteEndpoint remoteEndpoint = Mockito.mock(RemoteEndpoint.class);
    UserPC userPC = new UserPC(Commands.GENERAL_CHAT.toString(), message);
    GeneralChatService generalChatService = new GeneralChatService(gson, sessionCache, chatsHistoryDAO);
    private ConcurrentHashMap<UUID, LoginAndSession> cache = new ConcurrentHashMap<>();

    IncorrectMessageService cut = new IncorrectMessageService(gson, sessionCache);

    @Test
    public void sendMessageTest_MULTIPLE_USERS_ONLINE() throws IOException {
       /* UUID uuid1 = UUID.fromString("8af3633e-dfac-4956-a2b2-f6fa25476206");
        LoginAndSession loginAndSession1 = new LoginAndSession("login1");
        loginAndSession1.setSession(session);

        cache.put(uuid1, loginAndSession1);

        UUID uuid2 = UUID.fromString("8af3633e-dfac-4956-a2b2-f6fa25476206");
        LoginAndSession loginAndSession2 = new LoginAndSession("login1");
        loginAndSession1.setSession(session);

        cache.put(uuid2, loginAndSession2);

        UUID userUUID = UUID.fromString("720b052c-93c4-4c99-9843-ae4619f19ff7");
        LoginAndSession userLoginAndSession = new LoginAndSession("userLogin");
        userLoginAndSession.setSession(session);

        cache.put(userUUID, userLoginAndSession);

        Mockito.when(sessionCache.getCache()).thenReturn(cache);
        Mockito.when(session.getRemote()).thenReturn(remoteEndpoint);
        cut.sendMessage(message, userUUID);


        Mockito.verify(remoteEndpoint, times(1)).sendString(gson.toJson(userPC));
        Assertions.assertTrue(cut.sendMessage(message, userUUID));*/
    }

    @Test
    public void sendMessageTest_ONE_USER_ONLINE() throws IOException {

        /*UUID userUUID = UUID.fromString("720b052c-93c4-4c99-9843-ae4619f19ff7");
        LoginAndSession userLoginAndSession = new LoginAndSession("userLogin");
        userLoginAndSession.setSession(session);

        cache.put(userUUID, userLoginAndSession);

        Mockito.when(sessionCache.getCache()).thenReturn(cache);
        Mockito.when(session.getRemote()).thenReturn(remoteEndpoint);
        cut.sendMessage(message, userUUID);

        Mockito.verify(remoteEndpoint, times(1)).sendString(gson.toJson(userPC));
        Assertions.assertTrue(cut.sendMessage(message, userUUID));*/
    }

    @Test
    public void sendMessageTest_NO_USERS_ONLINE() throws IOException {
      /*  UUID currUserUUID = UUID.fromString("720b052c-93c4-4c99-9843-ae4619f19ff7");
        Session currSession = Mockito.mock(Session.class);
        //UserPC userPC = new UserPC(INCORRECT_MESSAGE.toString(), message);
   *//*     ConcurrentHashMap<UUID, LoginAndSession> cache = new ConcurrentHashMap<>();
        cache.put(currUserUUID, loginAndSession);*//*
        Mockito.when(sessionCache.getCache()).thenReturn(cache);
        Mockito.when(currSession.getRemote()).thenReturn(remoteEndpoint);
        Mockito.when(cache.get(currUserUUID)).thenReturn(loginAndSession);
        Mockito.when(loginAndSession.getSession()).thenReturn(currSession);

        cut.sendMessage(message, currUserUUID);

        Assertions.assertFalse(cut.sendMessage(message, currUserUUID));*/
    }
}