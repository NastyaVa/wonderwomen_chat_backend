package server.services.websocket.subsidiary;

import cache.SessionCache;
import database.dao.impl.UserDAO;
import models.LoginAndSession;
import models.UserLoginAndIsOnline;
import org.eclipse.jetty.websocket.api.Session;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.mockito.Mockito;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

public class ChatUsersGetterTest {

    SessionCache sessionCache = Mockito.mock(SessionCache.class);
    ConcurrentHashMap<UUID, LoginAndSession> cache = new ConcurrentHashMap<>();
    Session session1 = Mockito.mock(Session.class);
    Session session2 = Mockito.mock(Session.class);
    Session session3 = Mockito.mock(Session.class);
    UserDAO userDAO = Mockito.mock(UserDAO.class);
    ChatUsersGetter cut = new ChatUsersGetter(sessionCache, userDAO);

    @Test
    public void getUsersListTest() {
        LoginAndSession loginAndSession1 = new LoginAndSession("login1");
        cache.put(UUID.randomUUID(), loginAndSession1);
        loginAndSession1.setSession(session1);
        LoginAndSession loginAndSession2 = new LoginAndSession("login2");
        cache.put(UUID.randomUUID(), loginAndSession2);
        loginAndSession1.setSession(session2);
        LoginAndSession loginAndSession3 = new LoginAndSession("login3");
        cache.put(UUID.randomUUID(), loginAndSession3);
        loginAndSession1.setSession(session3);
        List<String> allUsersLogins = new ArrayList<>(Arrays.asList("login1", "login2", "login3", "login4", "login5"));
        UserLoginAndIsOnline userLoginAndIsOnline1 = new UserLoginAndIsOnline("login1");
        userLoginAndIsOnline1.setOnline(true);
        UserLoginAndIsOnline userLoginAndIsOnline2 = new UserLoginAndIsOnline("login2");
        userLoginAndIsOnline2.setOnline(true);
        UserLoginAndIsOnline userLoginAndIsOnline3 = new UserLoginAndIsOnline("login3");
        userLoginAndIsOnline3.setOnline(true);
        UserLoginAndIsOnline userLoginAndIsOnline4 = new UserLoginAndIsOnline("login4");
        UserLoginAndIsOnline userLoginAndIsOnline5 = new UserLoginAndIsOnline("login5");
        List<UserLoginAndIsOnline> expected = new ArrayList<>(Arrays.asList(userLoginAndIsOnline1, userLoginAndIsOnline2, userLoginAndIsOnline3, userLoginAndIsOnline4, userLoginAndIsOnline5));
        Mockito.when(sessionCache.getCache()).thenReturn(cache);
        Mockito.when(userDAO.getAllUserLogins()).thenReturn(allUsersLogins);
        Assertions.assertEquals(expected, cut.getUsersList());
    }
}