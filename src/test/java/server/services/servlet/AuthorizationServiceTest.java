package server.services.servlet;

import cache.SessionCache;
import com.google.gson.Gson;
import database.dao.impl.UserDAO;
import database.subsidiary.HashPassword;
import models.ResponseData;
import models.User;
import org.json.simple.JSONObject;
import org.junit.Test;
import org.mockito.Mockito;
import utils.validation.UserDataValidation;
import java.util.UUID;
import static org.junit.jupiter.api.Assertions.assertEquals;

public class AuthorizationServiceTest {

    private final JSONObject jsonResponse = new JSONObject();
    private final JSONObject jsonTooLongPassword = new JSONObject();
    private final JSONObject jsonUnacceptableCharsInPassword = new JSONObject();
    private final Gson gson = new Gson();
    private final SessionCache sessionCache = Mockito.mock(SessionCache.class);
    private final HashPassword hashPassword = Mockito.mock(HashPassword.class);
    private final UserDAO userDAO = Mockito.mock(UserDAO.class);
    private final UserDataValidation validationUser = new UserDataValidation();
    private final AuthorizationService authorizationService= new AuthorizationService(gson, userDAO, validationUser, sessionCache, hashPassword);

    @Test
    public void testRequestProcessing_INCORRECT_LOGIN_TEST() {
        jsonResponse.put("login", "123");
        jsonResponse.put("password", "12345678");
        String jsonTooShortPasswordString = jsonResponse.toJSONString();

        jsonTooLongPassword.put("login", "12345678qwertyuh");
        jsonTooLongPassword.put("password", "12345678");
        String jsonTooLongLoginString = jsonTooLongPassword.toJSONString();

        jsonUnacceptableCharsInPassword.put("login", "fkdl;*dskl");
        jsonUnacceptableCharsInPassword.put("password", "12345678");
        String jsonUnacceptableCharsInPasswordString = jsonUnacceptableCharsInPassword.toJSONString();

        ResponseData responseData = new ResponseData(403, gson.toJson("Login must consist from letters and digits, and contain from 4 to 15 characters."));

        ResponseData actualTooShortLogin = authorizationService.requestProcessing(jsonTooShortPasswordString);
        ResponseData actualTooLongLogin = authorizationService.requestProcessing(jsonTooLongLoginString);
        ResponseData actualUnacceptableCharsInLogin = authorizationService.requestProcessing(jsonUnacceptableCharsInPasswordString);

        assertEquals(actualTooShortLogin, responseData);
        assertEquals(actualTooLongLogin, responseData);
        assertEquals(actualUnacceptableCharsInLogin, responseData);
    }

    @Test
    public void requestProcessing_INCORRECT_PASSWORD_TEST() {
        jsonResponse.put("login", "user123");
        jsonResponse.put("password", "1234567");
        String jsonTooShortPasswordString = jsonResponse.toJSONString();

        jsonTooLongPassword.put("login", "user");
        jsonTooLongPassword.put("password", "12345678qwertyu7");
        String jsonTooLongPasswordString = jsonTooLongPassword.toJSONString();

        jsonUnacceptableCharsInPassword.put("login", "user");
        jsonUnacceptableCharsInPassword.put("password", "tuui");
        String jsonUnacceptableCharsInPasswordString = jsonUnacceptableCharsInPassword.toJSONString();

        ResponseData responseData = new ResponseData(403, gson.toJson("Password must consist from letters, digits or special characters, and contain from 8 to 15 characters."));

        ResponseData actualTooShortPassword = authorizationService.requestProcessing(jsonTooShortPasswordString);
        ResponseData actualTooLongPassword = authorizationService.requestProcessing(jsonTooLongPasswordString);
        ResponseData actualUnacceptableCharsInPassword = authorizationService.requestProcessing(jsonUnacceptableCharsInPasswordString);

        assertEquals(responseData.getMessage(), actualTooShortPassword.getMessage());
        assertEquals(responseData.getMessage(), actualTooLongPassword.getMessage());
        assertEquals(responseData.getMessage(), actualUnacceptableCharsInPassword.getMessage());
    }

    @Test
    public void testRequestProcessing_LOGIN_AND_PASS_FOUND() {
        String login = "user";
        String password = "5434d58u";
        String newHashPassword = "d23015cac44602ae85fe6afea9f61d7b6a25a68987a9702e915840076a434d58";
        jsonResponse.put("login", login);
        jsonResponse.put("password", password);
        String jsonResponseString = jsonResponse.toJSONString();

        UUID uuid = UUID.fromString("16fbeee8-84ef-11eb-8dcd-0242ac130003");
        User user = new User(login, newHashPassword , "email@mail.com", "092 9292 92 92", "company" );

        UserDataValidation validationUser = Mockito.mock(UserDataValidation.class);
        Mockito.when(validationUser.checkLogin(login)).thenReturn(true);
        Mockito.when(validationUser.checkPassword(password)).thenReturn(true);
        Mockito.when(userDAO.findByLogin(login)).thenReturn(user);
        Mockito.when(hashPassword.getHash(password)).thenReturn(newHashPassword);
        Mockito.when(userDAO.authorization(login)).thenReturn(newHashPassword);
        Mockito.when(sessionCache.addCache(login)).thenReturn(uuid);

        ResponseData expected = new ResponseData(200, gson.toJson(uuid));
        ResponseData actual = authorizationService.requestProcessing(jsonResponseString);

        assertEquals(expected.getMessage(), actual.getMessage());
    }

    @Test
    public void testRequestProcessing_LOGIN_NOT_FOUND() {
        String login = "user";
        String password = "12345678";
        jsonResponse.put("login", login);
        jsonResponse.put("password", password);
        String jsonResponseString = jsonResponse.toJSONString();

        UserDataValidation validationUser = Mockito.mock(UserDataValidation.class);
        UserDAO userDAO = Mockito.mock(UserDAO.class);
        Mockito.when(validationUser.checkLogin(Mockito.anyString())).thenReturn(true);
        Mockito.when(validationUser.checkPassword(Mockito.anyString())).thenReturn(true);
        Mockito.when(userDAO.findByLogin(login)).thenReturn(null);

        ResponseData expected = new ResponseData(403, gson.toJson("Login or password doesn't match our records."));

        assertEquals(expected.getMessage(), authorizationService.requestProcessing(jsonResponseString).getMessage());
    }

    @Test
    public void testRequestProcessing_PASSWORD_NOT_FOUND() {
        String login = "user";
        String password = "12345678";
        jsonResponse.put("login", login);
        jsonResponse.put("password", password);
        String jsonResponseString = jsonResponse.toJSONString();

        User user = new User(login, password, "email@mail.com", "380509940732", null);
        UserDataValidation validationUser = Mockito.mock(UserDataValidation.class);
        UserDAO userDAO = Mockito.mock(UserDAO.class);
        Mockito.when(validationUser.checkLogin(Mockito.anyString())).thenReturn(true);
        Mockito.when(validationUser.checkPassword(Mockito.anyString())).thenReturn(true);
        Mockito.when(userDAO.findByLogin(login)).thenReturn(user);
        Mockito.when(hashPassword.getHash(password)).thenReturn("d23015cac44602ae85fe6afea9f61d7b6a25a68987a9702e915840076a434d58");
        Mockito.when(userDAO.authorization(login)).thenReturn(null);

        ResponseData expected = new ResponseData(403, gson.toJson("Login or password doesn't match our records."));

        assertEquals(expected.getMessage(), authorizationService.requestProcessing(jsonResponseString).getMessage());
    }
}