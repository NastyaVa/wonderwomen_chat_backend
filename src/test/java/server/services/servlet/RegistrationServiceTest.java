package server.services.servlet;

import cache.GetNewID;
import cache.SessionCache;
import com.google.gson.Gson;
import database.subsidiary.ConnectionProvider;
import database.dao.impl.UserDAO;
import database.subsidiary.HashPassword;
import database.subsidiary.RandomPassword;
import models.ResponseData;
import org.json.simple.JSONObject;
import org.junit.Test;
import org.mockito.Mockito;
import utils.validation.UserDataValidation;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static utils.constants.ConstantsUserData.*;

public class RegistrationServiceTest {

    private JSONObject jsonResponseShort = new JSONObject();
    private JSONObject jsonResponseLong = new JSONObject();
    private JSONObject jsonResponseUnacceptableChars = new JSONObject();

    private Gson gson = new Gson();
    private ConnectionProvider connectionProvider = new ConnectionProvider();
    private HashPassword hashPassword = Mockito.mock(HashPassword.class);
    private RandomPassword randomPassword = Mockito.mock(RandomPassword.class);
    private UserDAO userDAO = new UserDAO(connectionProvider,hashPassword, randomPassword);
    private GetNewID getNewID = new GetNewID();
    private SessionCache sessionCache = new SessionCache(getNewID);
    private UserDataValidation validationUser = new UserDataValidation();

    private RegistrationService registrationService = new RegistrationService(gson, userDAO, validationUser, sessionCache);

    @Test
    public void testRequestProcessing_INCORRECT_LOGIN() {
        jsonResponseShort.put(LOGIN, "us");
        jsonResponseShort.put(PASSWORD, "djljf686667");
        jsonResponseShort.put(EMAIL, "gnju@gmail.com");
        jsonResponseShort.put(PHONE_NUMBER, "+380(67)777-7-777");
        jsonResponseShort.put(COMPANY_NAME, "Company");
        String jsonTooShortLoginString = jsonResponseShort.toJSONString();

        jsonResponseLong.put(LOGIN, "useruseruseruseruseruseruser");
        jsonResponseLong.put(PASSWORD, "djljf686667");
        jsonResponseLong.put(EMAIL, "gnju@gmail.com");
        jsonResponseLong.put(PHONE_NUMBER, "+380(67)777-7-777");
        jsonResponseLong.put(COMPANY_NAME, "Company");
        String jsonTooLongLoginString = jsonResponseLong.toJSONString();

        jsonResponseUnacceptableChars.put(LOGIN, "user{}");
        jsonResponseUnacceptableChars.put(PASSWORD, "djljf686667");
        jsonResponseUnacceptableChars.put(EMAIL, "gnju@gmail.com");
        jsonResponseUnacceptableChars.put(PHONE_NUMBER, "+380(67)777-7-777");
        jsonResponseUnacceptableChars.put(COMPANY_NAME, "Company");
        String jsonUnacceptableCharsInLoginString = jsonResponseUnacceptableChars.toJSONString();

        ResponseData responseData = new ResponseData(403, gson.toJson("Login must consist from letters and digits, and contain from 4 to 15 characters."));

        ResponseData actualTooShortLogin = registrationService.requestProcessing(jsonTooShortLoginString);
        ResponseData actualTooLongLogin = registrationService.requestProcessing(jsonTooLongLoginString);
        ResponseData actualUnacceptableCharsInLogin = registrationService.requestProcessing(jsonUnacceptableCharsInLoginString);

        assertEquals(actualTooShortLogin, responseData);
        assertEquals(actualTooLongLogin, responseData);
        assertEquals(actualUnacceptableCharsInLogin, responseData);
    }

    @Test
    public void testRequestProcessing_INCORRECT_PASSWORD() {
        jsonResponseShort.put(LOGIN, "user");
        jsonResponseShort.put(PASSWORD, "dj");
        jsonResponseShort.put(EMAIL, "gnju@gmail.com");
        jsonResponseShort.put(PHONE_NUMBER, "+380(67)777-7-777");
        jsonResponseShort.put(COMPANY_NAME, "Company");
        String jsonTooShortPSWString = jsonResponseShort.toJSONString();

        jsonResponseLong.put(LOGIN, "user");
        jsonResponseLong.put(PASSWORD, "djljf686667djljf686667");
        jsonResponseLong.put(EMAIL, "gnju@gmail.com");
        jsonResponseLong.put(PHONE_NUMBER, "+380(67)777-7-777");
        jsonResponseLong.put(COMPANY_NAME, "Company");
        String jsonTooLongPSWString = jsonResponseLong.toJSONString();

        jsonResponseUnacceptableChars.put(LOGIN, "user");
        jsonResponseUnacceptableChars.put(PASSWORD, "ufjgihjh ");
        jsonResponseUnacceptableChars.put(EMAIL, "gnju@gmail.com");
        jsonResponseUnacceptableChars.put(PHONE_NUMBER, "+380(67)777-7-777");
        jsonResponseUnacceptableChars.put(COMPANY_NAME, "Company");
        String jsonUnacceptableCharsInPSWString = jsonResponseUnacceptableChars.toJSONString();

        ResponseData responseData = new ResponseData(403, gson.toJson("Password must consist from letters, digits or special characters, and contain from 8 to 15 characters."));

        ResponseData actualTooShortPSW = registrationService.requestProcessing(jsonTooShortPSWString);
        ResponseData actualTooLongPSW = registrationService.requestProcessing(jsonTooLongPSWString);
        ResponseData actualUnacceptableCharsInPSW = registrationService.requestProcessing(jsonUnacceptableCharsInPSWString);

        assertEquals(actualTooShortPSW, responseData);
        assertEquals(actualTooLongPSW, responseData);
        assertEquals(actualUnacceptableCharsInPSW, responseData);
    }

    @Test
    public void testRequestProcessing_INCORRECT_EMAIL() {
        jsonResponseUnacceptableChars.put(LOGIN, "user");
        jsonResponseUnacceptableChars.put(PASSWORD, "djljf686667");
        jsonResponseUnacceptableChars.put(EMAIL, "gnju[]@gmail.com");
        jsonResponseUnacceptableChars.put(PHONE_NUMBER, "+380(67)777-7-777");
        jsonResponseUnacceptableChars.put(COMPANY_NAME, "Company");
        String jsonUnacceptableCharsInEmailString = jsonResponseUnacceptableChars.toJSONString();

        ResponseData responseData = new ResponseData(403, gson.toJson("Email entered incorrectly."));

        ResponseData actualUnacceptableCharsInEmail = registrationService.requestProcessing(jsonUnacceptableCharsInEmailString);

        assertEquals(actualUnacceptableCharsInEmail, responseData);
    }

    @Test
    public void testRequestProcessing_INCORRECT_PHONE() {
        jsonResponseShort.put(LOGIN, "user");
        jsonResponseShort.put(PASSWORD, "djljf686667");
        jsonResponseShort.put(EMAIL, "gnju@gmail.com");
        jsonResponseShort.put(PHONE_NUMBER, "+380(67)7");
        jsonResponseShort.put(COMPANY_NAME, "Company");
        String jsonTooShortPhoneString = jsonResponseShort.toJSONString();

        jsonResponseLong.put(LOGIN, "user");
        jsonResponseLong.put(PASSWORD, "djljf686667");
        jsonResponseLong.put(EMAIL, "gnju@gmail.com");
        jsonResponseLong.put(PHONE_NUMBER, "+380(67)777-7-777-7777-7777-777");
        jsonResponseLong.put(COMPANY_NAME, "Company");
        String jsonTooLongPhoneString = jsonResponseLong.toJSONString();

        jsonResponseUnacceptableChars.put(LOGIN, "user");
        jsonResponseUnacceptableChars.put(PASSWORD, "djljf686667");
        jsonResponseUnacceptableChars.put(EMAIL, "gnju@gmail.com");
        jsonResponseUnacceptableChars.put(PHONE_NUMBER, "+380/67/777/7/777");
        jsonResponseUnacceptableChars.put(COMPANY_NAME, "Company");
        String jsonUnacceptableCharsInPhoneString = jsonResponseUnacceptableChars.toJSONString();

        ResponseData responseData = new ResponseData(403, gson.toJson("The phone must contain only numbers. Special characters available '- _():=+'."));

        ResponseData actualTooShortPhone = registrationService.requestProcessing(jsonTooShortPhoneString);
        ResponseData actualTooLongPhone = registrationService.requestProcessing(jsonTooLongPhoneString);
        ResponseData actualUnacceptableCharsInPhone = registrationService.requestProcessing(jsonUnacceptableCharsInPhoneString);

        assertEquals(actualTooShortPhone, responseData);
        assertEquals(actualTooLongPhone, responseData);
        assertEquals(actualUnacceptableCharsInPhone, responseData);
    }

    @Test
    public void testRequestProcessing_INCORRECT_Company() {
        jsonResponseShort.put(LOGIN, "user");
        jsonResponseShort.put(PASSWORD, "djljf686667");
        jsonResponseShort.put(EMAIL, "gnju@gmail.com");
        jsonResponseShort.put(PHONE_NUMBER, "+380(67)777 77 77");
        jsonResponseShort.put(COMPANY_NAME, "Co");
        String jsonTooShortCompanyString = jsonResponseShort.toJSONString();

        jsonResponseLong.put(LOGIN, "user");
        jsonResponseLong.put(PASSWORD, "djljf686667");
        jsonResponseLong.put(EMAIL, "gnju@gmail.com");
        jsonResponseLong.put(PHONE_NUMBER, "+380(67)7777777");
        jsonResponseLong.put(COMPANY_NAME, "CompanyCompanyCompany");
        String jsonTooLongCompanyString = jsonResponseLong.toJSONString();

        jsonResponseUnacceptableChars.put(LOGIN, "user");
        jsonResponseUnacceptableChars.put(PASSWORD, "djljf686667");
        jsonResponseUnacceptableChars.put(EMAIL, "gnju@gmail.com");
        jsonResponseUnacceptableChars.put(PHONE_NUMBER, "+380(67)7777777");
        jsonResponseUnacceptableChars.put(COMPANY_NAME, "Company!");
        String jsonUnacceptableCharsInCompanyString = jsonResponseUnacceptableChars.toJSONString();

        ResponseData responseData = new ResponseData(403, gson.toJson("The company name can contain letters and numbers. Special character available '-'."));

        ResponseData actualTooShortCompany = registrationService.requestProcessing(jsonTooShortCompanyString);
        ResponseData actualTooLongCompany = registrationService.requestProcessing(jsonTooLongCompanyString);
        ResponseData actualUnacceptableCharsInCompany = registrationService.requestProcessing(jsonUnacceptableCharsInCompanyString);

        assertEquals(actualTooShortCompany, responseData);
        assertEquals(actualTooLongCompany, responseData);
        assertEquals(actualUnacceptableCharsInCompany, responseData);
    }
}