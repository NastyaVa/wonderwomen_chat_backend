package models;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class UserTest {
    private final String login1 = "bkodm67";
    private final String login2 = "1bko5dm7";
    private final String login3 = "UfROb5";

    private final String password1 = "djljf686667";
    private final String password2 = "584930200";
    private final String password3 = "1bJo5dm78%";

    private final String email1 = "gnju@gmail.com";
    private final String email2 = "vndu@me.co.uk";
    private final String email3 = "gkpv@yahoo.com";

    private final String phone1 = "+380(67)777-7-777";
    private final String phone2 = "+1(999)999-99-99";
    private final String phone3 = "+8(111)22-333-44";

    private final String company1 = "Company";
    private final String company2 = "CompName4j";
    private final String company3 = "Comp Name";

    private final User user1 = new User(login1, password1, email1, phone1, company1);
    private final User user2 = new User(login2, password2, email2, phone2, company2);
    private final User user3 = new User(login3, password3, email3, phone3, company3);
    private final User userNull = new User(password1, password1, email1, phone1, null);
    private final User userSpaces = new User(login1, password1, email2, phone3, "     ");

    @Test
    public void testEquals() {
        assertEquals(user1, user1);
        assertEquals(user2, user2);
        assertEquals(user3, user3);
        assertEquals(userNull, userNull);
        assertEquals(userSpaces, userSpaces);


        assertNotEquals(null, user1);
        assertNotEquals(user1, user2);
        assertNotEquals(user2, user3);
        assertNotEquals(user3, userNull);
        assertNotEquals(userNull, userSpaces);
        assertNotEquals(userSpaces, user1);
    }

    @Test
    public void testHashCode() {
        assertEquals(user1.hashCode(), user1.hashCode());
        assertEquals(user2.hashCode(), user2.hashCode());
        assertEquals(user3.hashCode(), user3.hashCode());
        assertEquals(userNull.hashCode(), userNull.hashCode());
        assertEquals(userSpaces.hashCode(), userSpaces.hashCode());

        assertNotEquals(user1.hashCode(), user2.hashCode());
        assertNotEquals(user2.hashCode(), user3.hashCode());
        assertNotEquals(user3.hashCode(), userNull.hashCode());
        assertNotEquals(userNull.hashCode(), userSpaces.hashCode());
        assertNotEquals(userSpaces.hashCode(), user1.hashCode());
    }
}