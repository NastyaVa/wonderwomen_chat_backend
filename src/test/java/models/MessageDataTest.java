package models;

import org.junit.Assert;
import org.junit.Test;
import utils.enums.Commands;

import java.util.UUID;

import static org.junit.Assert.*;

public class MessageDataTest {

    private final Message message1 = new Message( "User1", "User2", "Text1",165900);
    private final Message message2 = new Message( "User4", "User3", "Text2",150000);
    private final Message message3 = new Message( "User5", "User6", "Text3",111938);
    private final Message messageEmptyPayload = new Message( "User1", "User2", "",165900);
    private final Message messageEmptyUser = new Message( "User1", "", "Text",165900);
    private final Message messageNull = new Message( "User1", "User2", null,56789);

    private final UUID uuid1 = UUID.fromString("3500f61e-ee6a-4d3d-89ee-125ffa546e70");
    private final UUID uuid2 = UUID.fromString("3500f62e-fe6a-4d3d-89ee-126ffa546e79");
    private final UUID uuid3 = UUID.fromString("3500f61e-ee6a-5d3d-87ee-130ffa546e70");

    private final MessageData messageData1 = new MessageData(message1,uuid1,Commands.DEFAULT_COMMAND);
    private final MessageData messageData2 = new MessageData(message2, uuid2, Commands.DEFAULT_COMMAND);
    private final MessageData messageData3 = new MessageData(message3,uuid3,Commands.DEFAULT_COMMAND);
    private final MessageData messageData4 = new MessageData(message2, uuid1, Commands.DEFAULT_COMMAND);
    private final MessageData messageData5 = new MessageData(message2, uuid1, Commands.CHAT_USERS_GETTER);
    private final MessageData messageData6 = new MessageData(message3,uuid1,Commands.DEFAULT_COMMAND);
    private final MessageData messageData7 = new MessageData(messageEmptyPayload, uuid1, Commands.DEFAULT_COMMAND);
    private final MessageData messageData8 = new MessageData(messageEmptyUser,uuid1,Commands.DEFAULT_COMMAND);
    private final MessageData messageData9 = new MessageData(messageNull, uuid1, Commands.DEFAULT_COMMAND);


    @Test
    public void testEquals() {
        assertEquals(messageData1, messageData1);
        assertEquals(messageData2, messageData2);
        assertEquals(messageData3, messageData3);
        assertEquals(messageData4, messageData4);
        assertEquals(messageData5, messageData5);
        assertEquals(messageData6, messageData6);

        assertNotEquals(messageData1, messageData2);
        assertNotEquals(messageData2, messageData3);
        assertNotEquals(messageData3, messageData4);
        assertNotEquals(messageData1, messageData4);
        assertNotEquals(messageData4, messageData5);
        assertNotEquals(messageData5, messageData6);
        assertNotEquals(messageData6, messageData7);
        assertNotEquals(messageData6, messageData8);
        assertNotEquals(messageData6, messageData9);
    }

    @Test
    public void testHashCode() {
        assertEquals(messageData1.hashCode(), messageData1.hashCode());
        assertEquals(messageData2.hashCode(), messageData2.hashCode());
        assertEquals(messageData3.hashCode(), messageData3.hashCode());
        assertEquals(messageData4.hashCode(), messageData4.hashCode());
        assertEquals(messageData5.hashCode(), messageData5.hashCode());
        assertEquals(messageData6.hashCode(), messageData6.hashCode());

        assertNotEquals(messageData1.hashCode(), messageData2.hashCode());
        assertNotEquals(messageData2.hashCode(), messageData3.hashCode());
        assertNotEquals(messageData3.hashCode(), messageData4.hashCode());
        assertNotEquals(messageData1.hashCode(), messageData4.hashCode());
        assertNotEquals(messageData4.hashCode(), messageData5.hashCode());
        assertNotEquals(messageData5.hashCode(), messageData6.hashCode());
        assertNotEquals(messageData6.hashCode(), messageData7.hashCode());
        assertNotEquals(messageData6.hashCode(), messageData8.hashCode());
        assertNotEquals(messageData6.hashCode(), messageData9.hashCode());
    }

}