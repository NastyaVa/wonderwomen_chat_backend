package models;

import org.junit.Test;

import static org.junit.Assert.*;

public class ResponseDataTest {

    private final int codeInfo = 100;
    private final int codeSuccessful = 200;
    private final int codeRedirection = 300;
    private final int codeClientError = 400;
    private final int codeServerError = 500;

    private final String messageInfo = "Continue";
    private final String messageSuccessful = "OK";
    private final String messageRedirection = "Multiple Choice";
    private final String messageClientError = "Bad Request";
    private final String messageServerError = "Internal Server Error";

    private final ResponseData responseDataInfo = new ResponseData(codeInfo, messageInfo);
    private final ResponseData responseDataSuccessful = new ResponseData(codeSuccessful, messageSuccessful);
    private final ResponseData responseDataRedirection = new ResponseData(codeRedirection, messageRedirection);
    private final ResponseData responseDataClientError = new ResponseData(codeClientError, messageClientError);
    private final ResponseData responseDataServerError = new ResponseData(codeServerError, messageServerError);


    @Test
    public void testEquals() {
        assertEquals(responseDataInfo, responseDataInfo);
        assertEquals(responseDataSuccessful, responseDataSuccessful);
        assertEquals(responseDataRedirection, responseDataRedirection);
        assertEquals(responseDataClientError, responseDataClientError);
        assertEquals(responseDataServerError, responseDataServerError);

        assertNotEquals(null, responseDataInfo);
        assertNotEquals(responseDataInfo, responseDataSuccessful);
        assertNotEquals(responseDataSuccessful, responseDataRedirection);
        assertNotEquals(responseDataRedirection, responseDataClientError);
        assertNotEquals(responseDataClientError, responseDataServerError);
        assertNotEquals(responseDataServerError, responseDataInfo);
    }

    @Test
    public void testHashCode() {
        assertEquals(responseDataInfo.hashCode(), responseDataInfo.hashCode());
        assertEquals(responseDataSuccessful.hashCode(), responseDataSuccessful.hashCode());
        assertEquals(responseDataRedirection.hashCode(), responseDataRedirection.hashCode());
        assertEquals(responseDataClientError.hashCode(), responseDataClientError.hashCode());
        assertEquals(responseDataServerError.hashCode(), responseDataServerError.hashCode());

        assertNotEquals(responseDataInfo.hashCode(), responseDataSuccessful.hashCode());
        assertNotEquals(responseDataSuccessful.hashCode(), responseDataRedirection.hashCode());
        assertNotEquals(responseDataRedirection.hashCode(), responseDataClientError.hashCode());
        assertNotEquals(responseDataClientError.hashCode(), responseDataServerError.hashCode());
        assertNotEquals(responseDataServerError.hashCode(), responseDataInfo.hashCode());
    }
}