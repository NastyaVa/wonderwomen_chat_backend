package models;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import static org.junit.Assert.*;

public class UserLoginAndIsOnlineTest {
    private final String login1 = "login";
    private final String login2 = "bkodm67";
    private final String login3 = "1bko5dm7";
    private final String login4 = "UfROb5";

    private final UserLoginAndIsOnline userLoginAndIsOnline1 = new UserLoginAndIsOnline(login1);
    private final UserLoginAndIsOnline userLoginAndIsOnline2 = new UserLoginAndIsOnline(login2);
    private final UserLoginAndIsOnline userLoginAndIsOnline3 = new UserLoginAndIsOnline(login3);
    private final UserLoginAndIsOnline userLoginAndIsOnline4 = new UserLoginAndIsOnline(login4);
    private final UserLoginAndIsOnline userLoginAndIsOnlineNull = new UserLoginAndIsOnline(null);
    private final UserLoginAndIsOnline userLoginAndIsOnlineSpaces = new UserLoginAndIsOnline("    ");

    @Test
    public void testEquals() {
        assertEquals(userLoginAndIsOnline1, userLoginAndIsOnline1);
        assertEquals(userLoginAndIsOnline2, userLoginAndIsOnline2);
        assertEquals(userLoginAndIsOnline3, userLoginAndIsOnline3);
        assertEquals(userLoginAndIsOnline4, userLoginAndIsOnline4);
        assertEquals(userLoginAndIsOnlineNull, userLoginAndIsOnlineNull);
        assertEquals(userLoginAndIsOnlineSpaces, userLoginAndIsOnlineSpaces);

        assertNotEquals(userLoginAndIsOnline1, userLoginAndIsOnline2);
        assertNotEquals(userLoginAndIsOnline2, userLoginAndIsOnline3);
        assertNotEquals(userLoginAndIsOnline3, userLoginAndIsOnline4);
        assertNotEquals(userLoginAndIsOnline4, userLoginAndIsOnlineNull);
        assertNotEquals(userLoginAndIsOnlineNull, userLoginAndIsOnlineSpaces);
        assertNotEquals(userLoginAndIsOnlineSpaces, userLoginAndIsOnline1);
    }

    @Test
    public void testHashCode() {
        assertEquals(userLoginAndIsOnline1.hashCode(), userLoginAndIsOnline1.hashCode());
        assertEquals(userLoginAndIsOnline2.hashCode(), userLoginAndIsOnline2.hashCode());
        assertEquals(userLoginAndIsOnline3.hashCode(), userLoginAndIsOnline3.hashCode());
        assertEquals(userLoginAndIsOnline4.hashCode(), userLoginAndIsOnline4.hashCode());
        assertEquals(userLoginAndIsOnlineNull.hashCode(), userLoginAndIsOnlineNull.hashCode());
        assertEquals(userLoginAndIsOnlineSpaces.hashCode(), userLoginAndIsOnlineSpaces.hashCode());

        assertNotEquals(userLoginAndIsOnline1.hashCode(), userLoginAndIsOnline2.hashCode());
        assertNotEquals(userLoginAndIsOnline2.hashCode(), userLoginAndIsOnline3.hashCode());
        assertNotEquals(userLoginAndIsOnline3.hashCode(), userLoginAndIsOnline4.hashCode());
        assertNotEquals(userLoginAndIsOnline4.hashCode(), userLoginAndIsOnlineNull.hashCode());
        assertNotEquals(userLoginAndIsOnlineNull.hashCode(), userLoginAndIsOnlineSpaces.hashCode());
        assertNotEquals(userLoginAndIsOnlineSpaces.hashCode(), userLoginAndIsOnline1.hashCode());
    }
}