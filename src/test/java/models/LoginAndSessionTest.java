package models;

import org.junit.Test;

import static org.junit.Assert.*;

public class LoginAndSessionTest {
    private final LoginAndSession loginAndSession1 = new LoginAndSession("User1");
    private final LoginAndSession loginAndSession2 = new LoginAndSession("User2");
    private final LoginAndSession loginAndSession3 = new LoginAndSession("user");
    private final LoginAndSession loginAndSession4 = new LoginAndSession("USER");
    private final LoginAndSession loginAndSession5 = new LoginAndSession("USEr");

    private final LoginAndSession loginAndSessionNull = new LoginAndSession(null);
    private final LoginAndSession loginAndSessionSpaces = new LoginAndSession("     ");

    @Test
    public void testEquals() {
        assertEquals(loginAndSession1, loginAndSession1);
        assertEquals(loginAndSession2, loginAndSession2);
        assertEquals(loginAndSession3, loginAndSession3);
        assertEquals(loginAndSession4, loginAndSession4);
        assertEquals(loginAndSession5, loginAndSession5);

        assertNotEquals(loginAndSession1, loginAndSession2);
        assertNotEquals(loginAndSession2, loginAndSession3);
        assertNotEquals(loginAndSession3, loginAndSession4);
        assertNotEquals(loginAndSession4,loginAndSession5);
        assertNotEquals(loginAndSession1, loginAndSessionNull);
        assertNotEquals(loginAndSession1, loginAndSessionSpaces);
    }

    @Test
    public void testHashCode() {
        assertEquals(loginAndSession1.hashCode(), loginAndSession1.hashCode());
        assertEquals(loginAndSession2.hashCode(), loginAndSession2.hashCode());
        assertEquals(loginAndSession3.hashCode(), loginAndSession3.hashCode());
        assertEquals(loginAndSession4.hashCode(), loginAndSession4.hashCode());
        assertEquals(loginAndSession5.hashCode(), loginAndSession5.hashCode());

        assertNotEquals(loginAndSession1.hashCode(), loginAndSession2.hashCode());
        assertNotEquals(loginAndSession2.hashCode(), loginAndSession3.hashCode());
        assertNotEquals(loginAndSession3.hashCode(), loginAndSession4.hashCode());
        assertNotEquals(loginAndSession4.hashCode(), loginAndSession5.hashCode());
        assertNotEquals(loginAndSession1.hashCode(), loginAndSessionNull.hashCode());
        assertNotEquals(loginAndSession1.hashCode(), loginAndSessionSpaces.hashCode());

    }
}