package cache;

import models.LoginAndSession;
import org.eclipse.jetty.websocket.api.Session;
import org.eclipse.jetty.websocket.api.UpgradeRequest;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import java.util.*;
import static org.junit.jupiter.api.Assertions.*;

class CacheTest {

    static GetNewID getNewID = Mockito.mock(GetNewID.class);
    static SessionCache cut = new SessionCache(getNewID);
    static String login = "login";
    static String stringUUID = "3772fa40-81ca-11eb-8dcd-0242ac130003";
    static UUID uuid = UUID.fromString(stringUUID);
    static Session session = Mockito.mock(Session.class);
    Session sessionNotFound = Mockito.mock(Session.class);
    UpgradeRequest upgradeRequest = Mockito.mock(UpgradeRequest.class);
    Map parameterMap = Mockito.mock(Map.class);
    List list = Mockito.mock(List.class);


    @BeforeAll
    static void init() {
        LoginAndSession loginAndSession = new LoginAndSession(login);
        cut.getCache().put(uuid, loginAndSession);
        loginAndSession.setSession(session);
    }

    @Test
    void addCacheTest() {
        UUID uuid = UUID.fromString("0d22d3a2-174e-4f58-9dea-6758d5ad71c5");
        Mockito.when(getNewID.getUUID()).thenReturn(uuid);
        UUID actualUUID = cut.addCache(login);
        String actualLogin = cut.getCache().get(uuid).getLogin();
        assertEquals(login, actualLogin);
        assertEquals(uuid, actualUUID);
    }

    @Test
    void findByUUIDTest() {
        LoginAndSession expected = cut.getCache().get(uuid);
        LoginAndSession actual = cut.findByUUID(uuid);
        assertEquals(expected, actual);
    }

    @Test
    void setSessionTest_UUID_NOT_FOUND() {
        String uuidString = "b153b868-8fee-11eb-a8b3-0242ac130003";
        Mockito.when(session.getUpgradeRequest()).thenReturn(upgradeRequest);
        Mockito.when(upgradeRequest.getParameterMap()).thenReturn(parameterMap);
        Mockito.when(parameterMap.get("access-token")).thenReturn(list);
        Mockito.when(list.get(0)).thenReturn(uuidString);
        assertNull(cut.setSessionAndUUID(session));
    }

    @Test
    void setSessionTest_UUID_FOUND() {
        LoginAndSession loginAndSession = new LoginAndSession(login);
        cut.getCache().put(uuid, loginAndSession);
        Mockito.when(session.getUpgradeRequest()).thenReturn(upgradeRequest);
        Mockito.when(upgradeRequest.getParameterMap()).thenReturn(parameterMap);
        Mockito.when(parameterMap.get("access-token")).thenReturn(list);
        Mockito.when(list.get(0)).thenReturn(stringUUID);
        assertEquals(uuid, cut.setSessionAndUUID(session));
    }

    @Test
    void getUUIDBySessionTest_SESSION_FOUND() {
        assertEquals(uuid, cut.getUUIDBySession(session));
    }

    @Test
    void getUUIDBySessionTest_SESSION_NOT_FOUND() {
        assertNull(cut.getUUIDBySession(sessionNotFound));
    }

    @Test
    void removeByUUIDTest() {
        LoginAndSession loginAndSession = new LoginAndSession("login");
        UUID uuid = UUID.fromString("3116cc7b-253c-4eb7-8588-096709b256c5");
        cut.getCache().put(uuid, loginAndSession);
        cut.removeByUUID(uuid);
        assertNull(cut.getCache().get(uuid));
    }
}