package utils.validation;

import org.json.JSONObject;
import org.junit.Assert;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Set;

import static org.junit.Assert.*;
import static utils.constants.ConstantsMessage.PAYLOAD;

public class MessageValidationTest {

    MessageValidation messageValidation = new MessageValidation();
    String text = "{ \"to\" : \"TO\", \"from\" : \"FROM\", \"payload\" : \"PAYLOAD\", \"time\" : \"123456L\" }";
    JSONObject message = new JSONObject (text);

    @Test
    public void validateMessageContent() {
        assertFalse(messageValidation.validateMessageContent(message));
    }

    @Test
    public void validateMessageLength() {

    }
}