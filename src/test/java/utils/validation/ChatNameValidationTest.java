package utils.validation;

import org.junit.Test;
import org.junit.jupiter.api.Assertions;

import static org.junit.Assert.*;

public class ChatNameValidationTest {

    ChatNameValidation chatNameValidation = new ChatNameValidation();

    @Test
    public void validateChatName() {
        Assertions.assertTrue(chatNameValidation.validateChatName("chat"));
        Assertions.assertTrue(chatNameValidation.validateChatName("bkodm67"));
        Assertions.assertTrue(chatNameValidation.validateChatName("1bko5dm7"));
        Assertions.assertTrue(chatNameValidation.validateChatName("UfROb5"));

        Assertions.assertFalse(chatNameValidation.validateChatName(" "), "Error! Line is empty.");
        Assertions.assertFalse(chatNameValidation.validateChatName("uyo"), "Error! Line length from 4 to 15 characters.");
        Assertions.assertFalse(chatNameValidation.validateChatName("uyoovjrnthdertka"), "Error! Line length from 4 to 15 characters.");
        Assertions.assertFalse(chatNameValidation.validateChatName("ufj gi"), "Error! The line has a space. ");
        Assertions.assertFalse(chatNameValidation.validateChatName("ufjgi>"), "Error! Login contains extraneous character(s). ");
        Assertions.assertFalse(chatNameValidation.validateChatName("ваок"), "Error! Only Latin available");
    }
}