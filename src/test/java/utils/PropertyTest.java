package utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class PropertyTest {

    Property property = new Property();

    @Test
    public void testGetHost() {
        String actual = property.getHost();
        String expected = "jdbc:postgresql://ec2-63-34-97-163.eu-west-1.compute.amazonaws.com:5432/d6cmo87ohfih0m";

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testGetLogin() {
        String actual = property.getLogin();
        String expected = "esydtkgttnoxsh";

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testGetPassword() {
        String actual = property.getPassword();
        String expected = "0bcff5dd68a37f6f9d7dfeb72ed156ec89ccc4ebf1de87f50443a3097893a099";

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void testGetDriver() {
        String actual = property.getDriver();
        String expected = "org.postgresql.Driver";

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void getLoginMapping() {
        String actual = property.getLoginMapping();
        String expected = "/login";

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void getChatMapping() {
        String actual = property.getChatMapping();
        String expected = "/chat";

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void getRegistrationMapping() {
        String actual = property.getRegistrationMapping();
        String expected = "/registration";

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void getForgotPasswordMapping() {
        String actual = property.getForgotPasswordMapping();
        String expected = "/forgotPassword";

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void getPort() {
        String actual = property.getPort();
        String expected = "9080";

        Assertions.assertEquals(expected, actual);
    }

    @Test
    public void getSalt() {
        String actual = property.getSalt();
        String expected = "cni8gjrp0496jkgoek";

        Assertions.assertEquals(expected, actual);
    }
}