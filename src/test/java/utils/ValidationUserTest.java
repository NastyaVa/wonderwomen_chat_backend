package utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import utils.validation.UserDataValidation;

class ValidationUserTest {
    UserDataValidation validationUser = new UserDataValidation();

    @Test
    void checkLogin() {
        Assertions.assertTrue(validationUser.checkLogin("user"));
        Assertions.assertTrue(validationUser.checkLogin("bkodm67"));
        Assertions.assertTrue(validationUser.checkLogin("1bko5dm7"));
        Assertions.assertTrue(validationUser.checkLogin("UfROb5"));

        Assertions.assertFalse(validationUser.checkLogin(" "), "Error! Line is empty.");
        Assertions.assertFalse(validationUser.checkLogin("uyo"), "Error! Line length from 4 to 15 characters.");
        Assertions.assertFalse(validationUser.checkLogin("uyoovjrnthdertka"), "Error! Line length from 4 to 15 characters.");
        Assertions.assertFalse(validationUser.checkLogin("ufj gi"), "Error! The line has a space. ");
        Assertions.assertFalse(validationUser.checkLogin("ufjgi>"), "Error! Login contains extraneous character(s). ");
        Assertions.assertFalse(validationUser.checkLogin("ваок"), "Error! Only Latin available");
    }


    @Test
    void checkPassword() {
        Assertions.assertTrue(validationUser.checkPassword("djljf686667"));
        Assertions.assertTrue(validationUser.checkPassword("584930200"));
        Assertions.assertTrue(validationUser.checkPassword("1bJo5dm78%"));

        Assertions.assertFalse(validationUser.checkPassword(" "), "Error! Line is empty.");
        Assertions.assertFalse(validationUser.checkPassword("uyouyiu"), "Error! Line length from 8 to 15 characters.");
        Assertions.assertFalse(validationUser.checkPassword("uyoovjrnthdertka"), "Error! Line length from 8 to 15 characters.");
        Assertions.assertFalse(validationUser.checkPassword("ufjgihjh "), "Error! The line has a space. ");
    }

    @Test
    void checkEmail() {
        Assertions.assertTrue(validationUser.checkEmail("gnju@gmail.com"));
        Assertions.assertTrue(validationUser.checkEmail("vndu@me.co.uk"));
        Assertions.assertTrue(validationUser.checkEmail("gkpv@yahoo.com"));
        Assertions.assertTrue(validationUser.checkEmail("gnju@1.com"));
        Assertions.assertTrue(validationUser.checkEmail("vndu56@me.co.uk"));
        Assertions.assertTrue(validationUser.checkEmail("gkpv1@yahoo.com"));

        Assertions.assertFalse(validationUser.checkEmail("fhud@.com.ua"), "Error! Entered incorrect data.");
        Assertions.assertFalse(validationUser.checkEmail("fhu578d@.com"), "Error! Entered incorrect data.");
        Assertions.assertFalse(validationUser.checkEmail("ahug6d@.com.ru"), "Error! Entered incorrect data.");
        Assertions.assertFalse(validationUser.checkEmail("dvr^+%d@.com.ua"), "Error! Entered incorrect data.");
        Assertions.assertFalse(validationUser.checkEmail("fh76ud.com.ua"), "Error! Entered incorrect data.");
        Assertions.assertFalse(validationUser.checkEmail("phu@d@.com.ua"), "Error! Entered incorrect data.");
        Assertions.assertFalse(validationUser.checkEmail("yuud@.com.1ua"), "Error! Entered incorrect data.");
    }

    @Test
    void checkPhoneNumber() {
        Assertions.assertTrue(validationUser.checkPhoneNumber("+380(67)777-7-777"));
        Assertions.assertTrue(validationUser.checkPhoneNumber("+1(999)999-99-99"));
        Assertions.assertTrue(validationUser.checkPhoneNumber("+8(111)22-333-44"));
        Assertions.assertTrue(validationUser.checkPhoneNumber("071-878-134-1010"));
        Assertions.assertTrue(validationUser.checkPhoneNumber("+380 66 666 66 66"));

        Assertions.assertFalse(validationUser.checkPhoneNumber("+380(67)7"), "Error! Entered incorrect data.");
        Assertions.assertFalse(validationUser.checkPhoneNumber("+380(93)333 33 33 333"), "Error! Entered incorrect data.");
        Assertions.assertFalse(validationUser.checkPhoneNumber("071878"), "Error! Entered incorrect data.");
        Assertions.assertFalse(validationUser.checkPhoneNumber("+ghtfg8964"), "Error! Entered incorrect data.");
        Assertions.assertFalse(validationUser.checkPhoneNumber("+ghtf%&*g433g8964"), "Error! Entered incorrect data.");
    }

    @Test
    void checkCompanyName() {
        Assertions.assertTrue(validationUser.checkCompanyName("Company"));
        Assertions.assertTrue(validationUser.checkCompanyName("CompName4j"));
        Assertions.assertTrue(validationUser.checkCompanyName("Comp Name"));
        Assertions.assertTrue(validationUser.checkCompanyName("Comp-Name"));

        Assertions.assertFalse(validationUser.checkCompanyName("C"));
        Assertions.assertFalse(validationUser.checkCompanyName("Co-MPD*"));
        Assertions.assertFalse(validationUser.checkCompanyName("!CoTest!"));
        Assertions.assertFalse(validationUser.checkCompanyName("Rvdo%"));
        Assertions.assertFalse(validationUser.checkCompanyName("#Co+Pert"));
    }
}